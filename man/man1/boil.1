.\" Manpage for boil
.TH man 1 "19 February 2021" "Version 1.0-alpha" ""
.SH NAME
boil \- include boiler plate code in a shell script
.SH SYNOPSIS
.B boil
[-m MODULEDIR] [-n] [-d DEBUGLEVEL] [-o OPTIONS] [-t TARGETFILE] [-p PREFIX] [-F FILTER] [-S FACILITY] [-f] [-M KEY=VALUE] [MODULE ... ]
.br
.B boil
[-v]
.br
.B boil
[-h]

.SH DESCRIPTION
\fBBoil\fR is a tool that helps including code templates in \fBShell\fR scripts. It is an alternative - \fBnot\fR a replacement - to sourcing files. Files that contain general system functionality should be installed on the system and sourced as required.
.PP
An alternative to sourcing boilerplate code is to keep code snippets in dedicated files and copy/paste the content as required.
.PP
This tool assists with boilerplate code inclusion. The templates that one wishes to include can either be specified directly on the command line or they can be included via instructions in the script itself.
\fBBoil\fR is capable of detecting circular inclusions, thus avoiding getting stuck in an endless loop. It also keeps track of previously included templates to prevent multiple inclusion - unless explicitly instructed to allow multiple inclusion.

.SH OPTIONS
.TP
\fB\-m\fR MODULEDIR
Directory to look for modules. Default is $HOME/.boil
.TP
\fB\-n\fR
Do not use a default module. By default, \fBboil\fR outputs a default module, located in MODULEDIR/default.mod. Using \fB-n\fR suppresses this default output.
.TP
\fB\-d\fR DEBUGLEVEL
Controls \fBboil\fR's verbosity.
.TP
\fB-o\fR OPTIONS
Experimental feature. \fBBoil\fR can produce an options template. It requires a special template for that. If that template is provided then specifying this option will produce the options boiler plate code. The format of OPTIONS depends on which options template is used. E.g., for the \fBgetopts\fR template OPTIONS must be in the same format in which \fBgetopts\fR expects its options.
.TP
\fB-t\fR TARGETFILE
By default \fBboil\fR prints to stdout. This option redirects output to TARGETFILE. If TARGETFILE exists then \fBboil\fR exits with an error. This is useful to avoid accidental overwrites when manually redirecting stdout on the command line.
.TP
\fB-p\fR PREFIX
Experimental feature. Its syntax is likely to change in the future without prior notice.
Define a prefix in which to look for a module first. If the module is not found in the PREFIX path then look for it in the default locations.
.TP
\fB-F\fR FILTER
Activate or deactivate a certain filter. The syntax for FILTER is \fB<filtername>=<n>\fR, where \fB<n>\fR is either 0 (activate) or 1 (deactivate) and \fB<filtername>\fR is one of:
.br
.IP
* \fBcommment\fR: All commented lines will be removed. Default: \fBactive\fR.
.IP
* \fBhashbang\fR: All lines that contain a leading hashbang will be removed, with the exception of the first encountered one. The first encountered hashbang \fBwhile\fR the filter is active will be stored and output as the first line of the script. Default: \fBactive\fR.
.IP
The \fB-F\fR opion may be specified more than once.
.TP
\fB-S\fR FACILITY
Stop a processing facility. Possible values for \fBFACILITY\fR are:
.br
.IP
* \fBfilter\fR: Stop all filters
.IP
* \fBboilbang\fR: Stop processing boilbang instructions

.TP
\fB-f\fR
This option tells \fBboil\fR to ignore as many errors as possible.
.br
\fBWarning\fR: A file specified by \fB-t TARGETFILE\fR will also be overwritten if it exists. If a module is not found then boil keeps on processing the rest of the file. This may result in a corrupted script.
.TP
\fB-M\fR KEY=VALUE
Map a \fBKEY\fR to a user specified \fBVALUE\fR. The KEY/VALUE pair is passed in the form of \fBKEY=VALUE\fR and will be available in modules that have been inserted via \fBINSERT\fR and in \fBREPLACE\fR instructions. The syntax \fB$<KEY>\fR retrieves the mapped value\fBVALUE\fR for \fBKEY\fR.
.TP
\fB\-v\fR
print version and exit.
.TP
\fB\-h\fR
print help information and exit.

.PP
.SH USAGE
.PP
There are two ways to generate/include boilerplate code. The first one is to pass the scripts (modules) that contain the code on the command line as arguments

.PP
.RS
boil -n bash/mylib.mod bash/myotherlib.mod
.RE
.PP

will output the contents of $HOME/.boil/bash/mylib.mod and
$HOME/.boil/bash/myotherlib.mod to stdout.

.RS
boil -n -p prefix bash/mylib.mod
.RE
.PP

will first check if $HOME/.boil/prefix/bash/mylib.mod exists. If yes, then its contents will be output. Otherwise the contents of $HOME/.boil/bash/mylib.mod will be output to stdout.
.PP
If \fBboil\fR is called without the \fB-n\fR option then it will also include the default module which does not need to be defined explicitly on the commandline.
.PP
.RS
boil bash/mylib.mod
.RE
.PP
This will output the contents of $HOME/.boil/default.mod and $HOME/.boil/bash/mylib.mod to stdout. $HOME/.boil/default.mod can be an empty file in case default output is generally not wanted. This way the \fB-n\fR option does not always have to be provided explicitly.
.PP
.RS
boil -n ./relativelib.mod
.RE
.PP

will output the content of relativelib.mod if there is such a file in
the current directory.

.PP
.RS
boil -n /path/to/absolutelib.mod
.RE
.PP

will output the content of /path/to/absolutelib.mod if such a file
exists.

.SH Boilbang instructions
.PP
The other possibility to include boilerplate code is to issue instructions from within a script itself. An instruction begins with a \fBboilbang\fR which consists of the following three characters
.PP
.RS
#!>
.RE
.PP
The \fBboilbang\fR must be followed by an instruction. The currently recognized instructions are:
.PP
.IP
INCLUDE
.IP
MULTIPASS
.IP
MAP
.IP
REPLACE
.IP
INSERT
.IP
FILTER
.IP
START
.IP
STOP
.IP
RESTORE
.IP
GETOPTS (experimental)
.IP
NOP
.IP

.PP
.SH INCLUDE
The \fBINCLUDE\fR must be followed by the name of a template module. \fBINCLUDE\fR does not include any module more than once. The template parameter itself can have three forms:
.PP
.RS
#!> INCLUDE /absolute/path/to/lib.mod
.RE
.PP
The module at the absolute location /absolute/path/to/lib.mod will be included.
.PP
.RS
#!> INCLUDE ./relativelib.mod
.RE
.PP
The module at the explicit relative location ./relativelib.mod will be included. It should be noted that the location is relative to the location of the calling script.
.PP
.RS
#!> INCLUDE bash/mylib.mod
.RE
.PP
If no \fBPREFIX\fR is active then the above instruction will cause \fBboil\fR to look in the module directory for a module bash/mylib.mod. Unless otherwise specified by the \fB-m MODULEDIR\fR option, the default location for the module directory is \fB$HOME/.boil\fR. Hence, the above instruction includes a template module located at \fB$HOME/.boil/bash/mylib.mod\fR.
.PP
If \fB-p PREFIX\fR has been defined then \fBboil\fR will first look for \fB$HOME/.boil/PREFIX/bash/mylib.mod\fR. If the module is not found then it will try \fB$HOME/.boil/bash/mylib.mod\fR for inclusion.
.PP
.SH MULTIPASS
.PP
The \fBMULTIPASS\fR instruction behaves the same as the \fBINCLUDE\fR instrucions with the exception that it allows multiple inclusion of a module. Calling example:
.PP
.RS
#!> MULTIPASS /absolute/path/to/lib.mod
.RE
.PP
.RS
#!> MULTIPASS ./relativelib.mod
.RE
.PP
.RS
#!> MULTIPASS bash/mylib.mod
.RE
.PP

.SH MAP
.PP
Map a value to a key. In addition to the \fB-M\fR option a key/value pair can also be mapped inside the script with this instruction.

.RS
#!> MAP KEY=VALUE
.RE
.PP
Maps \fBVALUE\fR to \fBKEY\fR. The value can be retrieved in inserted modules or in \fBREPLACE\fR instructions.

.SH REPLACE
.PP
The argument is parsed and variables are replaced with their mapped values. If a key is not found then an error is raised. If the \fB-f\fR option has been supplied then the instrucion is ignored, i.e., the entire line will not be appended to the resulting script.

.RS
#!> REPLACE Replace 'KEY' with its '$<KEY>'
.RE
.PP
If \fBVALUE\fR has been mapped to \fBKEY\fR then the above instruction will result in
.PP
.RS
Replace 'key' with its 'value'
.RE
.PP
A syntax error or a non exising \fB$<KEY>\fR, as in
.PP
.RS
#!> REPLACE Replace 'key' with its '$<key'
.RE
.PP
will result in the entire line to be ignored (only if \fB-f\fR option has been defined).

.SH INSERT
.PP
The \fBINSERT\fR instruction will insert the specified module. The module can be either specified directly by its path or by a variable that has the path of the module mapped or by a combination of both.
.br
In contrast to the \fBINCLUDE\fR statement no instructions will be executed inside the inserted module. Variables, however, will still be automatically resolved whenever encountered.
.br
If a syntax error occurs inside an inserted module or if a key has no associated value then an error is raised. If the \fB-f\fR option has been defined the entire line is ignored and will not be appended to the resulting script. This behaviour may change in the future to only omit the variable.

.RS
#!> INSERT insertion.mod
.RE
.PP
If the module \fBinsertion.mod\fR exists in the default directory then it will be included. Otherwise an error will be raised. If the \fB-f\fR option is defined then the entire line will be ignored and not appended to the resulting script.

.RS
#!> INSERT ./insertion.mod
.RE
.PP
Inserts the module \fBinsertion.mod\fR in the current directory.

.RS
#!> INSERT /path/to/insertion.mod
.RE
.PP
Inserts the module at the absolute path \fB/path/to/insertion.mod\fR in the current directory.

.RS
#!> INSERT $<key>
.RE
.PP
The value of \fB$<key>\fR will be resolved and the mapped value will be interpreted as the module's path. The rules for paths as argument to \fBINSERT\fR apply.
.br
If there is no mapping for \fB$<key>\fR, however, then the \fBINSERT\fR instruction is ignored and no error will be raised in that case.

.SH FILTER
The syntax of the \fBFILTER\fR instruction is as follows

.RS
#!> FILTER <filtername>=<n>
.RE
.PP
where \fBfiltername\fR is the name of the filter (see \fB-F\fR option for possible values) and \fBn\fR is the status to set for the filter (0: \fBinactive\fR, 1: \fBactive\fR), e.g.,

.RS
#!> FILTER comment=1
.RE
.PP
will activate the comment filter, i.e., removing subsequent commented lines.
The filter will remain also active in any subsequently included submodules. It can be overriden in the submodules. The status of a filter is carried over into submodules but it will be never carried over to any parent module.
Consider the following two modules:
.PP
The contents of submodule.mod:

.br
.RS
# --- submodule.mod ---
.br
# comment removed by the parent module's filter status
.br
#!> FILTER comment=0
.br

# this comment will stay
.br
.RE

.br
The contents of parentmodule.mod:

.RS
# --- parentmodule.mod ---
.br
# activate comment filter
.br
#!> FILTER comment=1
.br

# this comment will be removed
.br

#!> INCLUDE ./submodule.mod
.br

# this comment will be deleted, too
.br
.RE

.PP
Assuming that there is no filter active when the parentmodule is processed, the comments until the first \fBboilbang\fR will be preserved. After the instruction

.RS
#!> FILTER comment=1
.RE
.PP
is issued, all subsequent comments will be deleted in the parentmodule. The first comments in submodule.mod will be deleted, too, since the filter has retained its \fBactive\fR status from the parent. After it is deactivated in the submodule all following comments in the submodule will be preserved. The deactivation, however, does not carry over to the parentmodule. Therefore, all comments in the parentmodule, that follow the \fBINCLUDE\fR instruction are still removed.


.SH START
Start functionality of \fBboil\fR.
Currently the following functionatlities are supported:
.PP
* \fBfilter\fR
.br
* \fBboilbang\fR

.PP
Examples:
.PP
.RS
#!> START filter
.RE
.PP
All filters are activated. This has the same effect as if each filter would have been activated manually, e.g., the filter status is carried over to submodules but not to parent modules.

.PP
.RS
#!> START boilbang
.RE
.PP
Processing of instructions marked with a 'boilbang' will be processed.

.SH STOP
Stop functionality of \fBboil\fR.
Currently the following functionatlities are supported:
.PP
* \fBfilter\fR
.br
* \fBboilbang\fR

.PP
Examples:
.PP
.RS
#!> STOP filter
.RE
.PP
All filters are deactivated. This has the same effect as if each filter would have been deactivated manually, e.g., the filter status is carried over to submodules but not to parent modules.
.PP
.RS
#!> STOP boilbang
.RE
.PP
Processing of instructions marked with a 'boilbang' will be suspended.

.SH RESTORE
Resumes functionality of \fBboil\fR.
Currently the following functionatlities are supported:
.PP
* \fBfilter\fR
.br
* \fBboilbang\fR
.PP
This instruction restores the previous state of a functionality. If this instruction is issued before
the functionality has been explicitly set then it has no effect because the last status of a
functionality is the same as the initial default status.

.SH GETOPTS
.PP
Experimental feature. It is still undecided if it will be kept.

.SH NOP
.PP
No operation. Mainly used for testing purposes but may be used for comments, too - if the need ever arises.

.SH KNOWN ISSUES
* \fBBoil\fR will not abort on most errors, e.g., if a file is not found it will keep processing, regardless if the \fB-f\fR flag has been specified or not.
.PP
* When no default module is found then \fBboil\fR will create one with minimal default content. This is not desirable. \fBBoil\fR shall not make any assumptions about the user's intentions. Processing should be aborted if any module is missing. This includes the default module.
.PP
* It is currently possible for a \fBKEY\fR to contain whitespace. While this does not raise an error it is also not the disired behaviour. This may change in the future and \fBKEY\fR names with whitespace will no longer be valid. It is therefore strongly discouraged to use \fBKEY\fR values with whitespace.
.PP

.SH AUTHOR
CRTS

