/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <stdlib.h>

#define BLANK " \t"
#define BLANK_LEN 2
/* there are more whitespace characters */
#define WHITESPACE " \t\n"
#define WHITESPACE_LEN 3

#define DUAL_DELIM "[]"

#define HEADER "header"
#define BODY "body"
#define FOOTER "footer"

#ifdef RELEASE
#define GROW_BY 25
#else
#define GROW_BY 1
#endif

struct stack2d {
	size_t pos;
	size_t size;
	void **array;
};

struct array2d {
	size_t  len;
	size_t capacity;
	void **array;
};

void dbg(int, char*, ...);

int grow_array(void**, size_t*, size_t, size_t);

int read_entire_file(char*, char*, char**, size_t*);

uint cat_str(char**, size_t*, char*, size_t);

void skip_chars_conditional(const char**, size_t*, const char*, size_t, int);
void skip_nchars(const char**, size_t*, size_t);
void skip_chars(const char**, size_t*, const char*, size_t);

void skip_order_chars(const char**, size_t*, const char*, size_t);
void skip_until_chars(const char**, size_t*, const char*, size_t);
void skip_whitespace(const char**, size_t*);
void skip_blank(const char**, size_t*);
void skip_until_whitespace(const char**, size_t*);
void skip_until_blank(const char**, size_t*);

void get_token(char**, size_t*, char**);
void get_dual_delim_token(char**, size_t*, const char*, char**);

struct kvMap* get_kvMap(uint quads, ...);
void free_kvMap(struct kvMap*);

char* multi_repstr(char, const char*, size_t, struct kvMap*);

void add_array_element2d(void*, struct array2d*);

void init_stack2d(struct stack2d* stack);
void push_stack2d(void*, struct stack2d*);
void* peek_stack2d(struct stack2d*);
void* pop_stack2d(struct stack2d*);
uint stack2d_contains(char*, struct stack2d*);
void release_stack2d_data(struct stack2d*);

/* TODO are Lists actually used or needed, at all? */
struct ListItem {
	struct ListItem* prev;
	struct ListItem* next;
	char* id; /* name of this item */
	char* data; /* actual data */
};

struct ListItem* insertListItem(char*, char*, struct ListItem*);
struct ListItem* appendListItem(char*, char*, struct ListItem*);
struct ListItem* removeListItem(struct ListItem*);
struct ListItem* findListItem(char*, struct ListItem*, uint);
struct ListItem* clearListItem(struct ListItem*);
struct ListItem* deleteListItem(struct ListItem*);

#endif /* UTIL_H_INCLUDED */
