/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


#include "../prepenv.h"
#include "../util.h"
#include "../instructions/map.h"

#include "getopts.h"


void set_module_pointers(char **mod, size_t *len, char **header, char **body, char **footer) {
	get_dual_delim_token(mod, len, DUAL_DELIM, header);

	if (*header == NULL || strcmp(*header, HEADER) != 0) {
		*header = NULL;
		*body = NULL;
		*footer = NULL;

		return;
	}

	/* skip to end of line */
	skip_until_chars((const char**) mod, len, "\n", 1);

	if (**mod == '\n')
		skip_nchars((const char**) mod, len, 1); /* skip first newline */

	/* point *header to content */
	*header = *mod;

	/* if the [header] was directly adjacent to [body] as in
	 * [header][body]
	 * then *header would be pointing to "[body]" now.
	 * the next call to get_dual_delim_token() would set
	 * the opening '[' of "body" to '\0', which
	 * would make *header an empty string. While above
	 * header/body pattern is discouraged, it will
	 * not result in an error.*/
	get_dual_delim_token(mod, len, DUAL_DELIM, body);

	if (*body == NULL || strcmp(*body, BODY) != 0) {
		*body = NULL;
		*footer = NULL;

		return;
	}

	/* skip to end of line */
	skip_until_chars((const char**) mod, len, "\n", 1);

	if (**mod == '\n')
		skip_nchars((const char**) mod, len, 1); /* skip first newline */

	/* point *body to content */
	*body = *mod;

	/* if [body] was directly adjacent to [footer] as in
	 * [body][footer]
	 * then *body would be pointing to "[footer]" now.
	 * the next call to get_dual_delim_token() would set
	 * the opening '[' of "footer" to '\0', which
	 * would make *body an empty string. While above
	 * body/footer pattern is discouraged, it will
	 * not result in an error.*/
	get_dual_delim_token(mod, len, DUAL_DELIM, footer);

	if (*footer == NULL || strcmp(*footer, FOOTER) != 0) {
		*footer = NULL;

		return;
	}

	/* skip to end of line */
	skip_until_chars((const char**) mod, len, "\n", 1);

	if (**mod == '\n')
		skip_nchars((const char**) mod, len, 1); /* skip first newline */

	/* point *footer to content */
	*footer = *mod;
}

void parse_getopts(char* mod, size_t len, char** result, const char* options, const char* varname) {
	*result = NULL;

	char *header = NULL, *body = NULL, *footer = NULL; /* pointers to content */

	set_module_pointers(&mod, &len, &header, &body, &footer);

	if (header == NULL || body == NULL || footer == NULL) {
		dbg(1, "Invalid module:\n%s\n", mod);

		return;
	}

	struct kvMap *map = get_kvMap(2,
			OPTS, strlen(OPTS), options, strlen(options),
			VAR, strlen(VAR), varname, strlen(varname));

	if (map == NULL) {
		dbg(1, "Failed to process options for getopts.\n");
		return;
	}

	*result = multi_repstr('%', header, strlen(header), map);
	free_kvMap(map);

	size_t reslen = strlen(*result);

	char *tmpres = NULL;

	char *opt = malloc(2); /* store single option as string */
	*(opt + 1) = '\0'; /* terminating '\0' will not change */
	for ( ; *options; options++) { /* traverse options string */
		/* user may have enclosed the options string with quotes */
		if (*options == ':' || *options == '\"' || *options == '\'')
			continue;

		*opt = *options;

		map = get_kvMap(1, O, strlen(O), opt, 1);
		if (map == NULL) {
			dbg(1, "Failed to process options for getopts.\n");
			return;
		}

		char *tmp = multi_repstr('%', body, strlen(body), map);
		free_kvMap(map);

		reslen += strlen(tmp);

		tmpres = realloc(*result, reslen + 1);

		if (tmpres == NULL)
			return;

		*result = tmpres;

		strcat(*result, tmp);

		free(tmp);
	}

	free(opt);

	reslen += strlen(footer);

	tmpres = realloc(*result, reslen + 1);
	if (tmpres == NULL)
		return;

	*result = tmpres;

	strcat(*result, footer);
}


