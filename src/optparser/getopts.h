/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GETOPTS_H_INCLUDED
#define GETOPTS_H_INCLUDED


#define OPTS "opts"
#define VAR "var"
#define O "o"


void set_module_pointers(char**, size_t*, char**, char**, char**);
void parse_getopts(char*, size_t, char**, const char*, const char*);

#endif /* GETOPTS_H_INCLUDED */
