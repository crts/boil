/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef START_H_INCLUDED
#define START_H_INCLUDED


/* definitions of suspension types */
#define S_FILTER "filter"
#define S_BOILBANG "boilbang"

#define START "START"
#define STOP "STOP"
#define RESTORE "RESTORE"


void start(char*);
void stop(char*);
void restore(char*);

#endif /* START_H_INCLUDED */
