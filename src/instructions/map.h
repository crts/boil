/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED


#define MAP "MAP"
#define REPLACE "REPLACE"



struct kvMap {
	size_t len;
	size_t capacity;
	char **key;
	char **val;
	size_t *klen;
	size_t *vlen;
};

void free_kvMap(struct kvMap*);
void truncate_map(struct kvMap* map, uint newlen);
void release_mapstack(struct stack2d*);
struct kvMap* get_kvMap(uint, ...);
uint kvMap_add_pair(struct kvMap* map, char* kv, size_t len);
size_t kvMap_getvalue(struct kvMap*, char*, char**);
size_t kvMap_parse_line(struct kvMap*, char**, size_t, char**);

#endif /* MAP_H_INCLUDED */
