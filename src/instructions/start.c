/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "start.h"
#include "../filter/filter.h"
#include "../util.h"
#include "../process.h"


void start(char *arg) {
	if (arg == NULL || *arg == '\0') {
		dbg(2, "Instruction '%s' requires an argument.\n", START);
		return;
	}

	if (strcmp(arg, S_FILTER) == 0) {
		activate_all_filters();
	} else if (strcmp(arg, S_BOILBANG) == 0) {
		activate_processors();
	} else {
		dbg(2, "Invalid argument for %s:%s\n", START, arg);
		return;
	}
}

void stop(char *arg) {
	if (arg == NULL || *arg == '\0') {
		dbg(2, "Instruction '%s' requires an argument.\n", STOP);
		return;
	}

	if (strcmp(arg, S_FILTER) == 0) {
		deactivate_all_filters();
	} else if (strcmp(arg, S_BOILBANG) == 0) {
		deactivate_processors();
	} else {
		dbg(2, "Invalid argument for %s:%s\n", STOP, arg);
		return;
	}
}

void restore(char *arg) {
	if (arg == NULL || *arg == '\0') {
		dbg(2, "Instruction '%s' requires an argument.\n", STOP);
		return;
	}

	if (strcmp(arg, S_FILTER) == 0) {
		restore_all_filters();
	} else if (strcmp(arg, S_BOILBANG) == 0) {
		restore_processors();
	} else {
		dbg(2, "Invalid argument for %s:%s\n", RESTORE, arg);
		return;
	}
}
