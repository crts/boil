/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include "../util.h"
#include "map.h"


#define ESCAPE '\\'
#define S_KEYSTART "$"
#define S_KEYOBRACE "<"
#define S_KEYCBRACE ">"



/* Returns the mapped value for <key>. The returned value must be
 * deallocated by the caller.
 */
size_t kvMap_getvalue(struct kvMap* map, char* key, char** value) {
	*value = NULL;

	uint i = (*map).len;
	while (i > 0) {
		i--;
		if (strcmp(*((*map).key + i), key) == 0) {
			*value = strdup(*((*map).val + i));
			return *((*map).vlen + i);
		}
	}

	return 0;
}

/* Resolve all keys to their mapped value in line.
 * The result with all keys resolved is stored in <target>.
 * Memory is automatically allocated for <target>, as needed.
 * In fact, <target> should not be allocated by the caller.
 * This function will mangle line.
 */
size_t kvMap_parse_line(struct kvMap* map, char** line, size_t len, char** target) {
	char* save = NULL;
	char* value = NULL;
	size_t vlen = 0;

	*target = NULL;

	size_t tlen = 0;

	save = *line;

	/* empty string */
	if (len == 0 && *line != NULL)
		if ( ! cat_str(target, &tlen, save, *line - save))
			goto ERROR;

	while (**(line) != '\0') {
		skip_until_chars((const char**) line, &len, S_KEYSTART, 1);

		if (*line - save) { /* chars skipped => (*line - save) > 0 */
			/* Either S_KEYSTART encountered or ESCAPE is last character
			 * of string. If ESCAPE is last character then it is not
			 * escaping anything. */
			if (*(*line - 1) == ESCAPE && **line != '\0') {
				/* Following sequence encountered:
				 *
				 *  ESCAPE KEYSTART => This is NOT an actual KEYSTART
				 *
				 * Replace ESCAPE character with current char. */
				*(*line - 1) = **line;

				/* Now the sequence looks like:
				 *
				 *  KEYSTART KEYSTART
				 *
				 * This will concat the strings <target> and <save>
				 * until the first KEYSTART */
				if ( ! cat_str(target, &tlen, save, *line - save))
					goto ERROR;

				/* skip redundant KEYSTART */
				(*line)++;
				len--;
			} else {
				if ( ! cat_str(target, &tlen, save, *line - save))
					goto ERROR;
			}

			save = *line;

			continue;
		}

		skip_order_chars((const char**) line, &len, S_KEYSTART S_KEYOBRACE, 2);

		if (*line - save) { /* chars have been skipped */
			save = *line;
			skip_until_chars((const char**) line, &len, S_KEYCBRACE, 1);
			if ( ! **line) { /* Error: Missing KEYCBRACE */
				dbg(1, "Error: Missing '" S_KEYCBRACE "'\n");
				goto ERROR;
			}

			**line = '\0'; /* make save valid string */
			(*line)++; /* move line to next char */
			len--;

			/* save now holds key */
			vlen = kvMap_getvalue(map, save, &value);

			if (value == NULL)
				goto ERROR;

			if ( ! cat_str(target, &tlen, value, vlen))
				goto ERROR;

			free(value);
			save = *line;
		} else {
			(*line)++;
			len--;
		}
	}

	if (*line != save) /* concat last part */
		if ( ! cat_str(target, &tlen, save, *line - save))
			goto ERROR;

	goto END;

	ERROR:
	if (*target != NULL) {
		free(*target);
		*target = NULL;
	}

	END:
	return tlen;
}

/* Parse <value> and map the result to <key>. This function will mangle
 * <value>, so pass a copy if you still need the value after it has been
 * mapped.
 */
static void kvMap_add(struct kvMap* map, char* key, size_t klen,
char* value, size_t valuelen) {
	if ((*map).len == (*map).capacity) {
		int tmp = (*map).capacity;
		grow_array((void**) &((*map).key), &(*map).capacity, GROW_BY, sizeof(*((*map).key)));
		(*map).capacity = tmp;
		grow_array((void**) &((*map).klen), &(*map).capacity, GROW_BY, sizeof(*((*map).klen)));
		(*map).capacity = tmp;
		grow_array((void**) &((*map).val), &(*map).capacity, GROW_BY, sizeof(*((*map).val)));
		(*map).capacity = tmp;
		grow_array((void**) &((*map).vlen), &(*map).capacity, GROW_BY, sizeof(*((*map).vlen)));
	}

	char* parsed = NULL;

	size_t plen = kvMap_parse_line(map, &value, valuelen, &parsed);

	uint l = (*map).len;
	*((*map).key + l) = strdup(key);
	*((*map).klen + l) = klen;
	*((*map).val + l) = parsed;
	*((*map).vlen + l) = plen;

	(*map).len++;
}

void free_kvMap(struct kvMap* map) {
	uint i;
	for (i = 0; i < (*map).len; i++) {
		free(*((*map).key + i));
		free(*((*map).val + i));
	}

	free((*map).key);
	free((*map).val);
	free((*map).klen);
	free((*map).vlen);

	free(map);
}

void truncate_map(struct kvMap* map, uint newlen) {
	while ((*map).len > newlen) {
		(*map).len--;
		free(*((*map).key + (*map).len));
		free(*((*map).val + (*map).len));
		*((*map).key + (*map).len) = NULL;
		*((*map).val + (*map).len) = NULL;
		*((*map).key + (*map).len) = 0;
		*((*map).val + (*map).len) = 0;
	}
}

/* Generates a key/value map. Each entry for this map is passed as a quartette
 * of paramaters:
 * - key (char*)
 * - keylength (uint)
 * - value (char*)
 * - valuelength (uint)
 *
 * The parameter 'quads' designates the total number of entries that are passed
 * to this function.
 *
 * Generated kvMap{} must be free()'ed by caller. */
struct kvMap* get_kvMap(uint quads, ...) {
	errno = 0;
	char **key = malloc(quads * sizeof(char*)); /* key strings */
	size_t *klen = malloc(quads * sizeof(size_t)); /* sizes of key strings */

	char **val = malloc(quads * sizeof(char*)); /* value strings */
	size_t *vlen = malloc(quads * sizeof(size_t)); /* sizes of value strings */

	if (key == NULL || klen == NULL || val == NULL || vlen == NULL)
		goto ABORT;

	va_list ap; /* point to variable arg list */
	va_start(ap, quads); /* init va_list */

	uint i;
	for (i = 0; i < quads; i++) {
		*(key + i) = strdup(va_arg(ap, char*));
		*(klen + i) = va_arg(ap, size_t);
		*(val + i) = strdup(va_arg(ap, char*));
		*(vlen + i) = va_arg(ap, size_t);
	}

	va_end(ap); /* cleanup va_list */

	errno = 0;
	struct kvMap *map = malloc(sizeof(struct kvMap));
	if (map == NULL)
		goto ABORT;

	(*map).len = quads;
	(*map).capacity = quads;
	(*map).key = key;
	(*map).klen = klen;
	(*map).val = val;
	(*map).vlen = vlen;

	return map;

	ABORT:
	if (errno)
		dbg(2, "Failed to create key/value map.\n");

	if (key != NULL) free(key);
	if (klen != NULL) free(klen);
	if (val != NULL) free(val);
	if (vlen != NULL) free(vlen);

	return NULL;
}

/* Adds the key/value pair <kv> of the form
 *
 *   key=value
 *
 * to <map>.
 */
uint kvMap_add_pair(struct kvMap* map, char* kv, size_t len) {
	const char* skipuntil = "=";
	char* k = kv;
	size_t kl = len;

	skip_until_chars((const char**) &kv, &len, skipuntil, 1);

	/* Error cases:
	 *  key_only or " " empty string
	 *  =value
	 *  key= */
	if (len == kl || len == 0) {
		dbg(1, "Invalid key/value pair: %s\n", k);
		return 0; /* false */
	}

	*kv = '\0';
	kv++;
	kl -= len;
	len--;

	kvMap_add(map, k, kl, kv, len);

	return 1;
}

