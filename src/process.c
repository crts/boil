/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


#include "prepenv.h"
#include "util.h"
#include "optparser/getopts.h"
#include "portability.h"
#include "filter/filter.h"
#include "instructions/start.h"
#include "instructions/map.h"

#include "process.h"


#define NUM_PROCESSORS 1

/* define processor positions */
#define F_MAIN 0

static struct sprocessor* processors = NULL;

struct sprocessor {
	int status;
	int laststatus;
};

static struct stack2d processorStack = {0, 0, NULL};

static char *processornames[NUM_PROCESSORS];
static size_t processornamesLen[NUM_PROCESSORS];


static const char* const BOILBANG = "#!>"; /* instruction marker */
static const int BOILBANG_LEN = 3; /* instruction marker length */

static const char* optstr = NULL; /* option string for generic options module */

static struct kvMap* varmap = NULL;

void process_module(const char*, uint, int);

extern struct stack2d pathStack;
extern struct stack2d pathStackInclude;
extern struct stack2d pathStackCircular;

struct stack2d outputBuffer;

int force = 0; /* boolean */

enum MarkerType {
	NONE,
	DISCARD,
	BOIL
};

static void set_status_processor(int idproc, int status) {
	(*(processors + idproc)).laststatus = (*(processors + idproc)).status;
	(*(processors + idproc)).status = status;
}

void activate_processors() {
	set_status_processor(F_MAIN, ACTIVE);
}

void deactivate_processors() {
	set_status_processor(F_MAIN, INACTIVE);
}

void restore_processors() {
	set_status_processor(F_MAIN, (*(processors + F_MAIN)).laststatus);
}

static void create_processors() {
	processors = malloc(sizeof(struct sprocessor) * NUM_PROCESSORS);

	/* call two times to set laststatus same as status */
	activate_processors();
	activate_processors();
}

void init_processors() {
	create_processors();

	processornames[F_MAIN] = S_MAIN;

	/* calculate lenghts of filter names */
	for (int i = 0; i < NUM_PROCESSORS; i++)
		processornamesLen[i] = strlen(processornames[i]);

}

static void copy_status_processor(struct sprocessor* templateProcs) {
	for (int i = 0; i < NUM_PROCESSORS; i++) {
		set_status_processor(i, (*(templateProcs + i)).status);
		(*(processors + i)).laststatus = (*(templateProcs + i)).laststatus;
	}
}

void push_processors() {
	struct sprocessor* oldProcs = processors;

	push_stack2d(oldProcs, &processorStack);

	create_processors();

	copy_status_processor(oldProcs);
}

void pop_processors() {
	free(processors);

	processors = pop_stack2d(&processorStack);
}

enum MarkerType set_instruction_pointers(char **line, size_t *slen, char **instPtr, char **argsPtr) {
	/* skip space and tab */
	skip_blank((const char**) line, slen);

	size_t oldlen = *slen;

	/* test if filters apply */
	if (apply_filters(line, slen))
		return DISCARD;

	/* test for boilbang marker */
	skip_order_chars((const char**) line, slen, BOILBANG, BOILBANG_LEN);

	/* no chars were skipped, hence no boilbang marker */
	if (*slen == oldlen)
		return NONE;

	/* filter instruction */
	get_token(line, slen, instPtr);

	/* filter argument */
	*argsPtr = *line;
	skip_until_chars((const char**) line, slen, "\n", 1);
	**line = '\0';

	return BOIL;
}

void buffer_output(char* line) {
	push_stack2d(strdup(line), &outputBuffer);
}

uint module_included(char* module) {
	return stack2d_contains(module, &pathStackInclude);
}

uint is_circular_inclusion(char* module) {
	return stack2d_contains(module, &pathStackCircular);
}

void process_instruction(char* line, size_t slen, int parseLines) {
	char *instPtr = NULL;
	char *argsPtr = NULL;

	char *saveLine = line;
	char* duplicate = NULL;

	/* Instruction processing deactivated, output
	 * instructions "as is".
	 * Backup line before it gets mangled */
	if ( (*(processors + F_MAIN)).status == INACTIVE)
		duplicate = strdup(line);

	enum MarkerType mt = NONE;
	/* this might mangle line */
	mt = set_instruction_pointers(&line, &slen, &instPtr, &argsPtr);

	switch (mt) {
	case NONE:
		if (parseLines) {
			char* parsed = NULL;
			kvMap_parse_line(varmap, &saveLine, slen, &parsed);
			if (parsed == NULL) {
				dbg(3, "Cannot parse:\n%s\n", saveLine);
			} else {
				buffer_output(parsed);

				/* free() <parsed> since it's duplicated by buffer_output() */
				free(parsed);
			}
		} else {
			buffer_output(saveLine);
		}

		break;
	case DISCARD:
		/* line has been filtered; do not include */
		break;
	case BOIL:
		/* <duplicate> not NULL when instruction processing disabled */
		if (duplicate != NULL) {
			if (strcmp(argsPtr, S_BOILBANG)
			|| (strcmp(instPtr, START) && strcmp(instPtr, RESTORE))) {
				/* no processing of boilbang instructions requested */
				buffer_output(duplicate);
				goto CLEANUP;
			}
		}

		if (strcmp(instPtr, INCLUDE) == 0) {
			if (argsPtr == NULL || *argsPtr == '\0') {
				dbg(2, "Instruction '%s' requires a module as an argument.\n", INCLUDE);
				goto CLEANUP;
			}

			process_module(argsPtr, 1, 0);
		} else if (strcmp(instPtr, MULTIPASS) == 0) {
			if (argsPtr == NULL || *argsPtr == '\0') {
				dbg(2, "Instruction '%s' requires a module as an argument.\n", MULTIPASS);
				goto CLEANUP;
			}

			process_module(argsPtr, 0, 0);
		} else if (strcmp(instPtr, INSERT) == 0) {
			char* parsed = NULL;
			char* argsbkp = strdup(argsPtr);
			kvMap_parse_line(varmap, &argsPtr, strlen(argsPtr), &parsed);
			if (parsed == NULL) {
				dbg(2, "INSERT failed. Cannot parse:\n%s\n", argsbkp);
			} else {
				stop(S_FILTER);
				stop(S_BOILBANG);

				process_module(parsed, 0, 1);

				restore(S_FILTER);
				restore(S_BOILBANG);
			}

			free(parsed);
			free(argsbkp);
		} else if (strcmp(instPtr, GETOPTS) == 0) {
			if (optstr == NULL) /* only if user set this option */
				goto CLEANUP;

			/* argsPtr holds name of module */
			if (argsPtr == NULL) {
				dbg(2, "Instruction '%s' requires an option module as argument.\n", GETOPTS);
				goto CLEANUP;
			}

			char *optnamePtr = NULL;
			char* temp = strdup(argsPtr);
			char* tempsave = temp;
			size_t tmplen = strlen(temp);
			get_token(&temp, &tmplen, &argsPtr); /* retrieve optional name for option variable */
			get_token(&temp, &slen, &optnamePtr); /* retrieve optional name for option variable */

			if (optnamePtr == NULL)
				optnamePtr = GETOPTS_DEFAULT_VARNAME;

			char *module = NULL;
			size_t tlen = 0;

			if (read_entire_file(argsPtr, "r", &module, &tlen) == -1) {
				dbg(2, "Failed to process %s module: %s\n", GETOPTS, argsPtr);
				goto CLEANUP_GETOPTS;
			}

			char *genericOpts = NULL;
			parse_getopts(module, tlen, &genericOpts, optstr, (const char*) optnamePtr);

			if (genericOpts != NULL) {
				buffer_output(genericOpts);
				free(genericOpts);
			}

			CLEANUP_GETOPTS:
			if (module != NULL)
				free(module);

			free(tempsave);
		} else if (strcmp(instPtr, I_FILTER) == 0) {
			parse_filter(argsPtr);
		} else if (strcmp(instPtr, START) == 0) {
			start(argsPtr);
		} else if (strcmp(instPtr, STOP) == 0) {
			stop(argsPtr);
		} else if (strcmp(instPtr, RESTORE) == 0) {
			restore(argsPtr);
		} else if (strcmp(instPtr, MAP) == 0) {
			kvMap_add_pair(varmap, argsPtr, strlen(argsPtr));
		} else if (strcmp(instPtr, REPLACE) == 0) {
			char* parsed = NULL;
			char* argsbkp = strdup(argsPtr);

			/* <parsed> will be allocated */
			kvMap_parse_line(varmap, &argsPtr, strlen(argsPtr), &parsed);
			if (parsed == NULL) {
				dbg(2, "REPLACE failed. Cannot parse:\n%s\n", argsbkp);
				if ( ! force)
					exit(EXIT_FAILURE);
			} else {
				size_t plen = strlen(parsed);
				cat_str(&parsed, &plen, "\n", 1);
				buffer_output(parsed);

				free(parsed);
			}

			free(argsbkp);
		} else {
			if (strcmp(instPtr, NOP) != 0)
				dbg(2, "Ignoring unknown instruction: %s\n", instPtr);
		}
		break;
	default:
		dbg(2, "Unhandled MarkerType: %d\n", (int) mt);
		break;
	}

	CLEANUP:
	/* <duplicate> will be duplicated in buffer_output(), free() required */
	if (duplicate != NULL)
		free(duplicate);

	return;
}

/* The purpose of this function is to resolve
 * relative paths which are used from within a
 * module.
 *
 * This function will mangle path. It will extract the
 * folderpath portion of the full path and make it
 * a valid string. Afterwards the filename will no
 * longer be part of the string.
 * Finally, the extracted path will be pushed onto the
 * pathstack, but only if there were any preceding
 * folders contained in the initial path.
 */
void push_path(char* path) {
	char *pathBegin = path;
	char *current = NULL;
	size_t len = strlen(path);

	do { /* advance to next dir separator with each iteration */
		current = path;
		skip_chars((const char**) &path, &len, DIR_S_SEP, 1);
		skip_until_chars((const char**) &path, &len, DIR_S_SEP, 1);
	} while (*path);

	/* push unmangled filepath to circular inclusion detection stack */
	push_stack2d(strdup(pathBegin), &pathStackCircular);

	if (*current == DIR_SEP) {
		*current = '\0'; /* truncate filename from path */

		push_stack2d(pathBegin, &pathStack);
	}
}

void process_module(const char *module, uint uniqInclude, int parseLines) {
	char *fullpath = (char*) get_conf_path(module);

	if (fullpath == NULL) {
		dbg(2, "Failed to process: %s\n", module);

		/* no allocated memory for fullpath; no need to call free() */
		return;
	}

	dbg(3, "Module: %s. Path: %s\n", module, fullpath);

	if (is_circular_inclusion(fullpath)) {
		dbg(2, "Circular module inclusion detected! Ignoring: %s\n", fullpath);

		goto END;
	}

	if (module_included(fullpath)) {
		if (uniqInclude) { /* multiple inclusion not allowed */
			dbg(3, "Omitting already included module: %s\n", fullpath);
			goto END;
		}
	} else {
		/* need copy because fullpath is free()'ed later */
		char* dup = NULL;
		dup = strdup(fullpath);
		push_stack2d(dup, &pathStackInclude);
	}

	errno = 0;
	FILE *istream = fopen(fullpath, "r");
	if (istream == NULL) {
		dbg(2, "Failed to open module file '%s': %s\n", fullpath, strerror(errno));
	} else {
		push_path(fullpath);

		char *line = NULL;
		size_t len = 0;
		ssize_t read;

		push_filters(); /* copy filters for submodules */
		push_processors(); /* copy processors for submodules */
		uint oldmaplen = (*varmap).len;

		while ((read = getline(&line, &len, istream)) != -1)
			process_instruction(line, (size_t) read, parseLines);

		truncate_map(varmap, oldmaplen);
		pop_processors(); /* restore processors for this module */
		pop_filters(); /* restore filters for this module */

		/* documentation of getline() states to free() *line
		 * even if the previous call to getline() failed */
		free(line);

		errno = 0;
		if (fclose(istream) != 0) /* error while closing module file */
			dbg(2, "I/O error while processing module file '%s': %s\n",
			fullpath, strerror(errno));

		/* will be free()'ed via fullpath */
		pop_stack2d(&pathStack);
		free(pop_stack2d(&pathStackCircular));
	}

	END:
	if (fullpath != NULL)
		free((void*) fullpath);

	return;
}

struct stack2d* build_script(struct options opts) {
	optstr = opts.optstr;

	varmap = opts.map;

	int i;
	for (i = 0; i < opts.listsize; i++)
		process_module(*(opts.list + i), 1, 0);

	/* reverse buffered lines on stack
	 * in order to return them in correct output order */
	char* line = NULL;
	struct stack2d* output = malloc(sizeof(struct stack2d));
	(*output).pos = 0;
	(*output).size = 0;
	(*output).array = NULL;

	while ((line = pop_stack2d(&outputBuffer)) != NULL) {
		push_stack2d(line, output);
	}

	release_stack2d_data(&processorStack);
	if (processors != NULL)
		free(processors);

	return output;
}
