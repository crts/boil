/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <unistd.h>


#include "portability.h"
#include "util.h"

#include "prepenv.h"


static const char *configDir;

struct stack2d pathStack = {0, 0, NULL};
struct stack2d pathStackInclude = {0, 0, NULL};
struct stack2d pathStackCircular = {0, 0, NULL};

struct array2d prefixBuffer;


/* Create folders
 */
int mkfolders(char *path, mode_t mode) {
	if (path == NULL || *path == '\0') {
		dbg(1, "Invalid or empty path for folder supplied: %s\n", path);
		return -1;
	}

	for (char *p = path + 1; *p; p++) {
		if (IS_DIR_SEP(*p)) {
			*p = '\0';

			errno = 0;
			/* create subfolder */
			if (mkdir(path, mode) && errno != EEXIST) {
				*p = DIR_SEP; /* restore separator */
				dbg(1, "Failed to create directory '%s': %s\n", path, strerror(errno));
				return -1;
			}

			*p = DIR_SEP; /* restore separator */
		}
	}

	errno = 0;
	/* create last subfolder */
	if (mkdir(path, mode) && errno != EEXIST) {
		dbg(1, "Failed to create directory '%s': %s\n", path, strerror(errno));
		return -1;
	}

	return 0;
}

/* Return value must be free()'ed by caller.
 */
char* catpath(const char *path, const char *append) {
	int pathlen = strlen(path);
	int len = pathlen + strlen(append) + 2; /* +2; DIR_SEP + '\0' */

	char *dst = NULL;
	dst = malloc(len * sizeof(char)); /* uninitialized */

	if (dst == NULL) {
		dbg(1, "Failed to allocate memory.\n");
	} else {
		strcpy(dst, path);
		*(dst + pathlen) = DIR_SEP; /* overwrite terminating '\0' */
		*(dst + pathlen + 1) = '\0'; /* make valid string again */
		strcat(dst, append);
	}

	return dst;
}

int test_module_file(const char *filename, const char *defContent) {
	int status = 0;

	errno = 0;
	FILE *file = fopen(filename, "r");

	if (file != NULL) { /* file exists; do not create default content */
		fclose(file);
		if (errno) {
			dbg(1, "Failed to close '%s' while checking for its existance\n", filename);
			status = errno;
		}

		return status;
	}

	errno = 0;
	/* create file */
	file = fopen(filename, "w");
	if (file == NULL) {
		dbg(1, "Error while creating default content for module.\n");

		return errno;
	}

	if (defContent != NULL && /* default content present */
			fputs(defContent, file) < 0) {
		dbg(1, "Error while creating default content.\n");
		status = -1;
	}

	errno = 0;
	fclose(file);
	if (errno) {
		dbg(1, "Created default module but an error occured when closing it: %s\n", strerror(errno));
		status = errno;
	}

	return status;
}

int init(char* modDir) {
	int status = 0;

	if (modDir == NULL) {
		const char *homeDir = NULL;

		if ((homeDir = getenv("HOME")) == NULL)
			homeDir = (*(getpwuid(getuid()))).pw_dir;

		configDir = catpath((const char*) homeDir, USER_MOD_DIR);

		char *moduleFile = catpath((const char*) configDir, DEF_MODULE_FILE);

		/* create folders if they do not exist */
		if ((status = mkfolders((char*) configDir, DEFMODE)) != 0)
			return status;

		if ((status = test_module_file(moduleFile, DEF_MODULE)) != 0)
			dbg(1, "Failed to initialize '%s'\n", moduleFile);

		free(moduleFile);
	} else {
		configDir = strdup(modDir);
	}

	return status;
}

extern struct stack2d filterStack;
extern struct sfilter* filters;

void release() {
	free((void*) configDir);
	release_stack2d_data(&pathStack);
	release_stack2d_data(&pathStackInclude);
	release_stack2d_data(&pathStackCircular);

	release_stack2d_data(&filterStack);
	if (filters != NULL)
		free(filters);
}

char* get_conf_path(const char *file) {
	/* NOTE: configDir is the only reason why this is in prepenv.c
	 * util.c makes more sense */

	char *abspath = NULL;
	char *prefixpath = NULL;

	/* do not search module directory if absolute or prefixed
	 * relative path (./path or ../path) to file is given */
	if (IS_DIR_SEP(*file)) { /* absolute path; do not modify */
		abspath = strdup(file);
	} else if (
	/* relative path referencing same directory */
	(*file == '.' && IS_DIR_SEP(*(file + 1))) ||
	/* relative path referencing parent directory */
	(*file == '.' && *(file + 1) == '.' && IS_DIR_SEP(*(file + 2))) ) {

		char *pre = peek_stack2d(&pathStack);

		if (pre == NULL) { /* nothing on stack yet; use as is */
			return realpath(file, NULL);
		} else {
			size_t prelen = strlen(pre);
			char *relpath = malloc(prelen + strlen(file) + 2); /* + DIR_SEP, + '\0' */

			/* OOM; abort */
			if (relpath == NULL) {
				dbg(2, "Out of memory while constructing relative path to module.\n");
				return NULL;
			}

			strcpy(relpath, pre);
			*(relpath + prelen) = DIR_SEP;
			*(relpath + prelen + 1) = '\0'; /* make valid string for subsequent strcat */
			strcat(relpath, file);
			abspath = realpath(relpath, NULL);

			free(relpath);
		}
	} else { /* path is relative to default module directory */
		if (prefixBuffer.len > 0) {
			FILE *istream = NULL;

			for (uint i = 0; i < prefixBuffer.len; i++) {
				prefixpath = catpath(*(prefixBuffer.array + i), file);
				if (prefixpath == NULL)
					goto END;

				abspath = catpath(configDir, prefixpath);
				free(prefixpath); /* not longer needed */
				if (abspath == NULL)
					goto END;

				/* test if file exists */
				istream = fopen(abspath, "r");
				if (istream != NULL) { /* file exists; use it */
					errno = 0;
					if (fclose(istream) != 0) /* error while closing module file */
						dbg(2, "I/O error while processing module file '%s': %s\n",
						abspath, strerror(errno));

					goto END;
				}

				/* release memory for invalid path */
				free(abspath);
				errno = 0;
			}
		}

		abspath = catpath(configDir, file);
	}

	END:
	return abspath;
}

