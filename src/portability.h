/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PORTABILITY_H_INCLUDED
#define PORTABILITY_H_INCLUDED

#ifdef WIN32

/* The directory separator on Windows is the '\' and the '/'.
 * The path separator (search locations for executables) is the semicolon.
 */
#define DIR_S_SEP "\\"
#define DIR_SEP '\\'
#define PATH_S_SEP ";"
#define PATH_SEP ';'
#define __TEST_SLASH_SEP || (s) == '/' /* additional test for slash on windows system */

#else  /* not WIN32 */

#define DIR_S_SEP "/"
#define DIR_SEP '/'
#define PATH_S_SEP ":"
#define PATH_SEP ':'
#define __TEST_SLASH_SEP /* ignore; only relevant for windows */

#endif /* not WIN32 */

#define IS_DIR_SEP(s) ((s) == DIR_SEP __TEST_SLASH_SEP)
#define IS_PATH_SEP(s) ((s) == PATH_SEP)

#endif /* PORTABILITY_H_INCLUDED */
