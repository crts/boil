/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>

#include "prepenv.h"
#include "boilconfig.h"
#include "process.h"
#include "util.h"
#include "filter/filter.h"
#include "instructions/start.h"
#include "instructions/map.h"


/* initial size of module list */
#define MODULE_LIST_SIZE 100

#define RELEASE_VERSION

static struct options opts;

extern int force; /* boolean */


extern struct stack2d outputBuffer;
extern struct array2d prefixBuffer;
extern int debuglevel;

extern char *hashbang;
extern int filterhashbang;
extern int filtercomment;

static void print_usage(char* progname) {
	fprintf(stderr, "Usage: %s [-m module_dir] [-n] [-d debuglevel]"
	" [-o options] [-t targetfile] [-p prefix] [-F filter] [-S type]"
	" [-f] [-v] [-h] [modules ... ]\n", progname);
}

int main(int argc, char **argv) {
	int opt;
	int pos = 1; /* default module is at position 0 */

	char *optstr = NULL;
	FILE *outstream = stdout; /* default output */
	char *targetfile = NULL;

	const char **list = NULL; /* module list */
	list = malloc(MODULE_LIST_SIZE * sizeof(char*)); /* uninitialized */
	*list = DEF_MODULE_FILE;

	/* alternative module folder; used for debugging */
	char *modDir = NULL;

	opts.map = get_kvMap(0);

	init_filters();
	init_processors();
	debuglevel = 1;
	while ((opt = getopt(argc, argv, "m:no:t:p:F:S:M:fvhV:")) != -1) {
		switch (opt) {
		case 'm': /* use as module directory */
			modDir = optarg;
			break;
		case 'n': /* do not use default module; overwrite default value */
			pos = 0;
			break;
		case 'o': /* experimental; may or may not be removed in the future */
			optstr = optarg;
			break;
		case 't':
			targetfile = optarg;
			break;
		case 'p': /* experimental; behaviour may change without prior notice */
			add_array_element2d(optarg, &prefixBuffer);
			break;
		case 'F': /* filter rule */
			parse_filter(optarg);
			break;
		case 'S': /* stop */
			stop(optarg);
			break;
		case 'M': /* map key to value */
			kvMap_add_pair(opts.map, optarg, strlen(optarg));
			break;
		case 'f': /* overwrite target file if it exists */
			force = 1;
			break;
		case 'V': /* print debug info; (V)erbosity */
			debuglevel = atoi(optarg);
			break;
		case 'v':
			fprintf(stdout, "%s %d.%d" RELEASE_VERSION "\n", *argv, boil_VERSION_MAJOR, boil_VERSION_MINOR);
			exit(EXIT_SUCCESS);
		case 'h':
			print_usage(*argv);
			exit(EXIT_SUCCESS);
		default: /* '?' */
			print_usage(*argv);
			exit(EXIT_FAILURE);
		}
	}

	/* FIXME calculate length of modules list and get rid of realloc() */
	/* append modules to list */
	while (optind < argc) {
		if (pos > 0 && (pos % MODULE_LIST_SIZE) == 0) {
			list = realloc(list, (pos + MODULE_LIST_SIZE) * sizeof(char*));
			if (list == NULL) {
				dbg(1, "Out of Memory.\n");
				exit(EXIT_FAILURE);
			}
		}

		*(list + pos++) = *(argv + optind++);
	}

	if (init(modDir) != 0)
		exit(EXIT_FAILURE);

	if (targetfile != NULL) {
		if ( ! force) {
			errno = 0;
			/* use file instead of stdout for output */
			outstream = fopen(targetfile, "r");
			if (outstream != NULL) {
				dbg(1, "Outputfile '%s' exists already. Use -f option to overwrite.\n", targetfile);
				fclose(outstream);
				if (errno)
					dbg(1, "Error while closing output file.\n");

				/* do NOT overwrite existing file */
				exit(EXIT_FAILURE);
			}
		}

		errno = 0;
		outstream = fopen(targetfile, "w"); /* truncate if exists */
		if (outstream == NULL) {
			dbg(1, "Failed to create file.\n");
			exit(EXIT_FAILURE);
		}
	}

	opts.list = list;
	opts.listsize = pos;
	opts.optstr = optstr;

	struct stack2d* script = build_script(opts);

	if (hashbang != NULL) {
		fprintf(outstream, "%s", hashbang);
		free(hashbang);
	}

	char* line = NULL;
	while ((line = pop_stack2d(script)) != NULL) {
		fprintf(outstream, "%s", line);
		free(line);
	}

	if (outstream != stdout) {
		errno = 0;
		fclose(outstream);
		if (errno) {
			dbg(1, "Error while closing output file.\n");
		}
	}

	/* free resources from *script */
	release_stack2d_data(script);

	/* unlike the other stacks *script has been malloc()'ed
	 * therefore it needs to be free()'ed */
	free(script);

	/* there are no more stack elements present, however,
	 * the array itself must still be free()'ed. */
	release_stack2d_data(&outputBuffer);

	/* the array's string elements themselves point
	 * to optarg which was aquired by getopt.
	 * They cannot be free()'ed. Just free()'ing the
	 * array is sufficient. */
	free(prefixBuffer.array);

	/* a <kvMap> needs a special release function. */
	free_kvMap(opts.map);

	release();
	free(list);

	exit(EXIT_SUCCESS);
}


