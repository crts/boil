/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROCESS_H_INCLUDED
#define PROCESS_H_INCLUDED


#define INCLUDE "INCLUDE"
#define MULTIPASS "MULTIPASS"
#define INSERT "INSERT"
#define GETOPTS "GETOPTS"
#define NOP "NOP"

#define GETOPTS_DEFAULT_VARNAME "opt"


/* not yet needed */
#define S_MAIN "main"

struct options {
	const char **list;
	int listsize;
	char *optstr;
	struct kvMap* map;
};

struct stack2d* build_script(struct options);

void activate_processors();
void deactivate_processors();
void restore_processors();
void init_processors();

#endif /* PROCESS_H_INCLUDED */
