/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PREPENV_H_INCLUDED
#define PREPENV_H_INCLUDED


/* define max path for this application */
#define MAX_PATH_SIZE 4096

#define USER_MOD_DIR ".boil"
#define DEF_MODULE_FILE "default.mod"
/* FIXME: do NOT make any assumptions about the default module */
#define DEF_MODULE "#!/bin/sh\n\n" /* if default module does not exist yet */

#define DEFMODE (S_IRWXU | S_IRWXG | S_IRWXO)


int init(char*);
int mkfolders(char*, mode_t);
char* get_conf_path(const char* file);
void release();

#endif /* PREPENV_H_INCLUDED */

