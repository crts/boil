/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>

#include "prepenv.h"

#include "util.h"
#include "instructions/map.h"


int debuglevel = 1;


void dbg(int level, char* format, ...) {
#ifdef RELEASE
	if (level > debuglevel)
		return;
#endif
	va_list ap; /* point to variable arg list */
	va_start(ap, format); /* init va_list */

	vfprintf(stderr, format, ap);

	va_end(ap); /* cleanup va_list */
}

uint cat_str(char** target, size_t* tlen, char* src, size_t slen) {
	*tlen = *tlen + slen;

	char* new = NULL;
	new = (char*) realloc(*target, (*tlen + 1)*sizeof(char)); /* sizeof(char) == 1 */
	if (new == NULL) {
		dbg(0, "Out of memory in function cat_str().\n");
		return 0;
	}

	if (*target == NULL)
		*new = '\0';

	*target = new;
	strncat(*target, src, slen);

	return 1;
}

int read_entire_file(char* filename, char* mode, char** result, size_t* rlen) {
	int status = -1;

	const char *filepath = get_conf_path(filename);

	errno = 0;
	FILE *file = fopen(filepath, mode);
	if (file == NULL) { /* errno set by fopen() */
		dbg(3, "Failed to open file '%s': %s\n", filepath, strerror(errno));
		goto CLEANUP;
	}

	getdelim(result, rlen, '\0', file); /* read entire file */
	if (errno) { /* getdelim() failed */
		dbg(3, "Error while reading file '%s': %s\n", filepath, strerror(errno));
		if (*result != NULL)
			free(*result);

		*result = NULL;
	} else {
		status = 0;
	}

	errno = 0;
	fclose(file);
	if (errno)
		dbg(3, "Error while closing file '%s': %s\n", filepath, strerror(errno));

	CLEANUP:
	if (errno)
		dbg(2, "Failed to read file '%s': %s\n", filepath, strerror(errno));

	if (filepath != NULL)
		free((void*) filepath);

	return status;
}

/* Skips at most n characters. If *slen < n then
 * only *slen characters will be skipped.
 * The string size is adjusted accordingly.
 */
void skip_nchars(const char **line, size_t *slen, size_t n) {
	if (n > *slen) n = *slen;

	*line += n;
	*slen -= n;

	return;
}

/* If (boolean) skip is true then adjust *line to point
 * to the first character that is NOT in skipchars.
 *
 * If (boolean) skip is false then adjust *line to point
 * to the first character that is contained in skipchars.
 *
 * In either case *slen is adjusted to the
 * new string length of adjusted *line.
 */
void skip_chars_conditional(const char **line, size_t *slen, const char *skipchars, size_t skiplen, int skip /* boolean */) {
	size_t i;
	size_t k;

	for (i = 0; i < *slen; i++) {
		for (k = 0; k < skiplen; k++)
			if (*(*line + i) == *(skipchars + k)) { /* skipchar found */
				if (skip) /* character must be skipped */
					goto CONTINUE; /* continue outer loop */
				else /* character must NOT be skipped */
					goto FOUND; /* break outer loop */
			}

		if (skip) /* no skipchars found; exit loop */
			break;

		/* continue skipping characters */
		CONTINUE:;
	}

	FOUND:
	skip_nchars(line, slen, i); /* move *line to first non-skipable character */

	return;
}

/* Adjust *line to point to the first character
 * that is NOT in skipchars.
 *
 * Adjust *slen to the new string length of adjusted *line.
 */
void skip_chars(const char **line, size_t *slen, const char *skipchars, size_t skiplen) {
	size_t i;
	size_t k;

	for (i = 0; i < *slen; i++) {
		for (k = 0; k < skiplen; k++)
			if (*(*line + i) == *(skipchars + k)) /* skipchar found */
				goto CONTINUE; /* continue outer loop */

		/* no skipchars found; exit loop */
		break;

		/* continue skipping characters */
		CONTINUE:;
	}

	skip_nchars(line, slen, i); /* move *line to first non-skipable character */

	return;
}

/* If the first orderlen chars in *line appear in the same
 * order as in orderchars, then *line is adjusted to
 * point to *(line + orderlen).
 *
 * The new string length *slen is also adjusted to
 * the length of adjusted *line.
 */
void skip_order_chars(const char **line, size_t *slen, const char *orderchars, size_t orderlen) {
	/* *line must not be shorter than orderchars */
	if (*slen < orderlen)
		return;

	size_t i;
	for (i = 0; i < orderlen; i++)
		if (*(*line + i) != *(orderchars + i))
			goto END; /* orderchars not at beginning of *line */

	skip_nchars(line, slen, orderlen); /* move *line to point to *(line + orderlen) */

	END:
	return;
}

/* Adjust *line to point to the first character
 * that is in untilchars.
 *
 * Adjust *slen to the new string length of adjusted *line
 */
void skip_until_chars(const char **line, size_t *slen, const char *untilchars, size_t untilen) {
	size_t i;
	size_t k;

	for (i = 0; i < *slen; i++)
		for (k = 0; k < untilen; k++)
			if (*(*line + i) == *(untilchars + k)) /* untilchar found */
				goto FOUND; /* break outer loop */

	FOUND:
	skip_nchars(line, slen, i); /* move *line to first non-skipable character */

	return;
}

/* Convenience function */
void skip_blank(const char **line, size_t *slen) {
	skip_chars(line, slen, BLANK, BLANK_LEN);
}

/* Convenience function */
void skip_whitespace(const char **line, size_t *slen) {
	skip_chars(line, slen, WHITESPACE, WHITESPACE_LEN);
}

/* Convenience function */
void skip_until_blank(const char **line, size_t *slen) {
	skip_until_chars(line, slen, BLANK, BLANK_LEN);
}

/* Convenience function */
void skip_until_whitespace(const char **line, size_t *slen) {
	skip_until_chars(line, slen, WHITESPACE, WHITESPACE_LEN);
}

/*
 */
void get_token(char **line, size_t *slen, char **token) {
	/* skip all whitespace (including newline) */
	skip_whitespace((const char**) line, slen);

	if (*slen == 0) /* no token */
		return;

	*token = *line;

	/* skip until next whitespace (including newline) */
	skip_until_whitespace((const char**) line, slen);

	if (*slen > 0) {
		**line = '\0'; /* terminate *token */
		skip_nchars((const char**) line, slen, 1); /* move line to valid char */
	}
}

void get_dual_delim_token(char **str, size_t *slen, const char *delim, char **token) {
	size_t init_len = *slen;
	char *init_str = *str;
	*token = NULL;

	skip_until_chars((const char**) str, slen, delim, 1);

	if (**str) { /* first delimiter found */
		/* save potential token position */
		*token = *str + 1; /* *str is pointing to delimiter */
		skip_until_chars((const char**) str, slen, delim + 1, 1);

	}

	if (**str == '\0') { /* at least one delimiter not found */
		/* restore */
		if (*token)
			*((*token) - 1) = *delim;

		*str = init_str;
		*slen = init_len;

		*token = NULL;
	} else {
		*(*token - 1) = '\0'; /* terminate string before token */

		/* terminate *token */
		**str = '\0';
		skip_nchars((const char**) str, slen, 1); /* adjust *str to point past last delimiter */
	}
}

/* Grows the number of elements in an array.
 * <size> and <by> refer to the number of elements and not
 * the total amount of memory needed. Therefore, both
 * parameters have to be multiplied with <typefactor>, i.e.,
 * the amount of memory required by one element.
 */
int grow_array(void** array, size_t* size, size_t growBy, size_t typefactor) {
	if (*size * typefactor == SIZE_MAX) { /* already maxed out */
		dbg(2, "Maximum array size (%zu elements) reached (%zu bytes).\n", *size, SIZE_MAX);
		errno = ENOMEM;
		return 0;
	}

	size_t newsize = *size + growBy;

	*size = (newsize * typefactor < *size * typefactor) /* overflow */
		? (SIZE_MAX / typefactor)
		: newsize;

	void *new = realloc(*array, *size * typefactor);

	if (new != NULL) {
		*array = new;

		return 1;
	}

	return 0;
}

/* Returns the number of bytes appended
 * FIXME growBy is probably not a good idea to be passed by caller
 */
size_t append_if_key_in_list(char** result, size_t offset, size_t* reslen, size_t growBy,
const char* key, size_t klen, struct kvMap* kv) {
	size_t vlen = 0;
	uint i, k;
	for (i = 0; i < (*kv).len; i++) {
		if (*((*kv).klen + i) != klen) /* impossible to match if sizes differ */
			continue;

		/* compares klen chars at max; no trailing '\0' expected,
		 * therefore strcmp() does not work here */
		for (k = 0; k < klen; k++)
			if (*(key + k) != *(*((*kv).key + i) + k))
				goto CONTINUE_OUTER; /* no match; continue outer loop */

		/* key matches */
		vlen = *((*kv).vlen + i); /* length of value to insert, excluding '\0' */

		if (offset + vlen > *reslen) /* array too small */
			if ( ! grow_array((void**) result, reslen, growBy < vlen ? vlen + 1 : growBy + 1, 1))
				return 0;

		(*reslen)--; /* size is string length without '\0' */

		/* copy value to result at offset */
		strcpy(*result + offset, *((*kv).val + i));

		break;

		CONTINUE_OUTER:;
	}

	return vlen;
}

/*
 */
char* multi_repstr(char fmt, const char* str, size_t slen, struct kvMap* map) {
	if (fmt == '\0')
		fmt = '%';

	/* FIXME use ONLY fmt as delimiter for replacement variable; NO WHITESPACE */
	size_t skiplen = WHITESPACE_LEN + 1; /* +1 fmt */
	char *skip = malloc(skiplen + 1);
	if (skip == NULL)
		goto END;

	*skip = fmt;
	*(skip + 1) = '\0';
	strcat(skip, WHITESPACE);

	/* FIXME start with "better" value; is allocation needed at this point? */
	size_t resultlen = 0;
	char *result = malloc(resultlen + 1);
	if (result == NULL)
		goto END;

	*result = '\0'; /* start with empty string */

	size_t respos = 0;

	/* boolean that remembers if an fmt char was encountered */
	int fmtstart = 0;

	const char *keyStr = str;
	size_t saveLen = slen;

	while (slen > 0) {
		errno = 0; /* grow_array() sets errno */
		if (respos == resultlen &&
		! grow_array((void**) &result, &resultlen, 2 * slen + 1, 1)) /* +1, '\0' */
			goto END;

		/* decrement to get string length without '\0' */
		resultlen--;

		if (*str == fmt) {
			fmtstart = !fmtstart;
		} else if (fmtstart) {
			fmtstart = 0;

			/* current position in str is key candidate */
			keyStr = str;
			saveLen = slen;
			skip_until_chars(&str, &slen, skip, skiplen);

			/* No need to append '\0' because append_if_key_in_list()
			 * requires the length to be passed. It does not utilize
			 * a trailing '\0' for its operation */
			errno = 0; /* call to append_if_key_in_list() sets errno */
			respos += append_if_key_in_list(&result, respos, &resultlen, 2 * slen,
			keyStr, saveLen - slen, map);

			if (errno)
				goto END;

			/* The key string can be either enlosed with the fmt
			 * char and a blank or within two fmt chars.
			 * If key was enclosed within fmt chars, e.g., %key%, then
			 * skip the terminating fmt char. */
			if (*str == fmt)
				goto ADVANCE;

			continue;
		}

		if ( ! fmtstart) /* do not copy single fmt char */
			*(result + respos++) = *str;

		ADVANCE:
		str++;
		slen--;
	}

	END:
	if (skip != NULL)
		free(skip);

	if (errno)
		dbg(2, "Multistring replace failed.\n");

	if (result != NULL)
		*(result + respos) = '\0'; /* make valid string */

	return result;
}

/* FIXME: if grow_array fails then do not just return; set some kind of error condition */
void add_array_element2d(void* element, struct array2d* arrayBuffer) {
	errno = 0; /* grow_array2d() sets errno */

	if ( (*arrayBuffer).len == (*arrayBuffer).capacity
	&& ! grow_array((void**) &((*arrayBuffer).array), &((*arrayBuffer).capacity),
	GROW_BY,  sizeof((*arrayBuffer).array)) ) {
		return;
	}

	*((*arrayBuffer).array + (*arrayBuffer).len++) = element;

	return;
}

void init_stack2d(struct stack2d* stack) {
	(*stack).array = NULL;
	(*stack).pos = 0;
	(*stack).size = 0;
}

/* FIXME: if grow_array fails then do not just return; set some kind of error condition */
void push_stack2d(void* data, struct stack2d* stack) {
	errno = 0; /* grow_array2d() sets errno */

	if ((*stack).pos == (*stack).size
	&& ! grow_array((void**) &((*stack).array), &((*stack).size),
	GROW_BY, sizeof((*stack).array)))
		return;

	*((*stack).array + (*stack).pos++) = data;
}

void* peek_stack2d(struct stack2d* stack) {
	return (*stack).pos > 0 ? *((*stack).array + (*stack).pos - 1) : NULL;
}

void* pop_stack2d(struct stack2d* stack) {
	return (*stack).pos > 0 ? *((*stack).array + --(*stack).pos) : NULL;
}

uint stack2d_contains(char* str, struct stack2d* stack) {
	size_t i;

	for (i = 0; i < (*stack).pos; i++) {
		char *ptr = *((*stack).array + i);
		if (strcmp(ptr, str) == 0)
			return 1;
	}

	return 0;
}

void release_stack2d_data(struct stack2d* stack) {
	/* free data */
	void *data = NULL;
	while ((data = pop_stack2d(stack)) != NULL)
		free(data);

	/* free data array */
	free((*stack).array);
}

struct ListItem* new_list_item() {
	struct ListItem* newItem = malloc(sizeof(struct ListItem));

	(*newItem).prev = NULL;
	(*newItem).next = NULL;
	(*newItem).id = NULL;
	(*newItem).data = NULL;

	return newItem;
}

struct ListItem* insertListItem(char* id, char* data, struct ListItem* item) {
	struct ListItem* newItem = new_list_item();

	(*newItem).id = id;
	(*newItem).data = data;

	if (item == NULL) /* first item in list */
		goto END;

	if ((*item).prev != NULL) {
		(*(*item).prev).next = item;
		(*newItem).prev = (*item).prev;
	}

	(*item).prev = newItem;
	(*newItem).next = item;

	END:
	return newItem;
}

struct ListItem* appendListItem(char* id, char* data, struct ListItem* item) {
	struct ListItem* newItem = new_list_item();

	(*newItem).id = id;
	(*newItem).data = data;

	if (item == NULL) /* first item in list */
		goto END;

	if ((*item).next != NULL) {
		(*(*item).next).prev = item;
		(*newItem).next = (*item).next;
	}

	(*item).next = newItem;
	(*newItem).prev = item;

	END:
	return newItem;
}

struct ListItem* removeListItem(struct ListItem* item) {
	if (item == NULL)
		return NULL;

	if ((*item).next != NULL)
		(*(*item).next).prev = (*item).prev;

	if ((*item).prev != NULL)
		(*(*item).prev).next = (*item).next;

	(*item).next = NULL;
	(*item).prev = NULL;

	return item;
}

struct ListItem* clearListItem(struct ListItem* item) {
	if (item == NULL)
		return NULL;

	if ((*item).id != NULL) {
		free((*item).id);
		(*item).id = NULL;
	}

	if ((*item).data != NULL) {
		free((*item).data);
		(*item).data = NULL;
	}

	return item;
}

struct ListItem* deleteListItem(struct ListItem* item) {
	if (item == NULL)
		return NULL;

	removeListItem(item);
	clearListItem(item);

	return item;
}
