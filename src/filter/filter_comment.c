/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>



/* There must be no leading whitespace characters
 * in line.
 */
int filter_comment(char **line, size_t *len) {
	if (*len > 0) /* *len is at least 1 */
		if (**line == '#')
			/* do no filter hashbang */
			if (*len == 1 || *(*line + 1) != '!')
				return 1;

	return 0;
}
