/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>

#include "availablefilters.h"
#include "../util.h"
#include "filter.h"


#define NUM_FILTERS 2

/* define filter positions */
#define F_COMMENT 0
#define F_HASHBANG 1



struct sfilter* filters = NULL;

struct sfilter {
	int status;
	int laststatus;
	/* filter function */
	int (*filter)(char**, size_t*);
};

struct stack2d filterStack = {0, 0, NULL};

static char *filternames[NUM_FILTERS];

static size_t filternamesLen[NUM_FILTERS];

static void set_status_filter(int idfilter, int status) {
	(*(filters + idfilter)).laststatus = (*(filters + idfilter)).status;
	(*(filters + idfilter)).status = status;
}

void parse_filter(char *filterExpression) {
	if (filterExpression == NULL || *filterExpression == '\0') {
		dbg(2, "Instruction '%s' requires an argument.\n", I_FILTER);
		return; /* FIXME exit if <force> not specified */
	}

	size_t len = strlen(filterExpression);
	size_t oldlen = len;

	/* save pointer to begining of filtername */
	char *filtername = filterExpression;

	/* find '=' sign */
	skip_until_chars((const char**) &filterExpression, &len, "=", 1);

	/* filtername must be followed by '=0' or '=1'.
	 * Therefore, the remaining length must be 2. */
	if (len != 2) {
		dbg(2, "Invalid filter expression: '%s'.\n", filterExpression);
		return; /* FIXME exit if <force> not specified */
	}

	size_t filterlen = oldlen - len;

	/* make <filtername> valid string by adding '\0' */
	*filterExpression = '\0';

	filterExpression++;

	int status = 0;
	switch (*filterExpression) {
	case '0':
		status = INACTIVE;
		break;
	case '1':
		status = ACTIVE;
		break;
	default:
		dbg(2, "Invalid filter value: %s.\n", filterExpression);
		/* FIXME exit if not force'd */
		return;
	}

	for (int i = 0; i < NUM_FILTERS; i++)
		if (filternamesLen[i] == filterlen)
			if (strcmp(filtername, filternames[i]) == 0) {
				set_status_filter(i, status);
				return;
			}

	return;
}

int apply_filters(char **line, size_t *slen) {
	int ret = 0;

	skip_blank((const char**) line, slen);

	for (int i = 0; i < NUM_FILTERS; i++)
		if ((*(filters + i)).status == ACTIVE
		&& (*(filters + i)).filter(line, slen))
			ret = 1;

	return ret;
}

static void set_function_filter(int idfilter, int (*filter)(char**,size_t*)) {
	(*(filters + idfilter)).filter = filter;
}

void activate_all_filters() {
	set_status_filter(F_COMMENT, ACTIVE);
	set_status_filter(F_HASHBANG, ACTIVE);
}

void deactivate_all_filters() {
	set_status_filter(F_COMMENT, INACTIVE);
	set_status_filter(F_HASHBANG, INACTIVE);
}

void restore_all_filters() {
	set_status_filter(F_COMMENT, (*(filters + F_COMMENT)).laststatus);
	set_status_filter(F_HASHBANG, (*(filters + F_HASHBANG)).laststatus);
}

static void create_filters() {
	filters = malloc(sizeof(struct sfilter) * NUM_FILTERS);

	set_function_filter(F_COMMENT, filter_comment);
	set_function_filter(F_HASHBANG, filter_hashbang);

	/* call two times to set laststatus same as status */
	activate_all_filters();
	activate_all_filters();
}

void init_filters() {
	create_filters();

	filternames[F_COMMENT] = S_COMMENT;
	filternames[F_HASHBANG] = S_HASHBANG;

	/* calculate lenghts of filter names */
	for (int i = 0; i < NUM_FILTERS; i++)
		filternamesLen[i] = strlen(filternames[i]);
}

static void copy_status_filter(struct sfilter* templateFilters) {
	for (int i = 0; i < NUM_FILTERS; i++) {
		set_status_filter(i, (*(templateFilters + i)).status);
		(*(filters + i)).laststatus = (*(templateFilters + i)).laststatus;
	}
}

void push_filters() {
	struct sfilter* oldFilters = filters;

	push_stack2d(oldFilters, &filterStack);

	create_filters();

	copy_status_filter(oldFilters);
}

void pop_filters() {
	free(filters);

	filters = pop_stack2d(&filterStack);
}
