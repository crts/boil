/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AVAILABLEFILTERS_H_INCLUDED
#define AVAILABLEFILTERS_H_INCLUDED

int filter_comment(char**, size_t*);
int filter_hashbang(char**, size_t*);
int filter_boilbang(char**, size_t*);

#endif /* AVAILABLEFILTERS_H_INCLUDED */
