/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILTER_H_INCLUDED
#define FILTER_H_INCLUDED

/* filter status */
#define INACTIVE 0
#define ACTIVE 1

/* 'I' as in Instruction */
#define I_FILTER "FILTER"

#define S_COMMENT "comment"
#define S_HASHBANG "hashbang"



void parse_filter(char*);

int apply_filters(char **line, size_t *slen);
void activate_all_filters();
void deactivate_all_filters();
void restore_all_filters();
void init_filters();
void push_filters();
void pop_filters();

#endif /* FILTER_H_INCLUDED */
