/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>


char *hashbang = NULL;

/* There must be no leading whitespace characters
 * in line.
 */
int filter_hashbang(char **line, size_t *len) {
	if (*len > 1) /* *len is at least 2 */
		if (**line == '#' && *(*line + 1) == '!')
			/* do no filter boilbang */
			if (*len == 2 || *(*line + 2) != '>') {
				if (hashbang == NULL)
					hashbang = strdup(*line);
				return 1;
			}

	return 0;
}
