/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_UTIL_H_INCLUDED
#define TEST_UTIL_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CUnit/Basic.h>


#include "../src/util.h"


#define DUAL_DELIM "[]"


typedef struct kvMap *kvMap;


void test_skip_chars(void);
void test_skip_until_chars(void);
void test_skip_order_chars(void);
void test_get_token(void);
void test_get_dual_delim_token(void);
void test_append_if_key_in_list(void);
void test_multi_repstr(void);

void skip_chars(const char**, size_t*, const char*, size_t);
void skip_until_chars(const char**, size_t*, const char*, size_t);
void skip_order_chars(const char**, size_t*, const char*, size_t);
void get_token(char**, size_t*, char**);
void get_dual_delim_token(char**, size_t*, const char*, char**);
size_t append_if_key_in_list(char**, size_t, size_t*, size_t, const char*, size_t, struct kvMap*);
char* multi_repstr(char, const char*, size_t, struct kvMap*);
struct kvMap* get_kvMap(uint, ...);
void free_kvMap(struct kvMap*);


#endif /* TEST_UTIL_H_INCLUDED */
