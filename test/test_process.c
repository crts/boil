/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_process.h"
#include "../src/filter/filter.h"
#include "../src/util.h"

extern int debuglevel;
extern struct stack2d filterStack;
extern struct sfilter* filters;


void test_set_instruction_pointers(void) {
	debuglevel = 1;
	char *moduleLines[] = {
		"",
		" ",
		"\t",
		" \t",
		"#",
		" #",
		"\t#",
		" \t#",
		"#-",
		" #-",
		"\t#-",
		" \t#-",
		"#!>",
		" #!>",
		"\t#!>",
		" \t#!>",
		"#!>instruction",
		" #!>instruction",
		"\t#!>instruction",
		" \t#!>instruction",
		"#!> instruction",
		" #!> instruction",
		"\t#!> instruction",
		" \t#!> instruction",

		"#!>instruction ",
		"#!> instruction\t",
		"#!> instruction\t ",
		"#!>instruction\n",
		"#!> instruction\n",
		"#!>instruction \n",
		"#!> instruction\t\n",
		"#!> instruction\t \n",

		"#!>instruction argument",
		"#!>instruction\targument",
		"#!>instruction \targument",
		"#!>instruction argument ",
		"#!>instruction\targument\t",
		"#!>instruction \targument\t ",

		"#!>instruction argument\n",
		"#!>instruction\targument\n",
		"#!>instruction \targument\n",
		"#!>instruction argument \n",
		"#!>instruction\targument\t\n",
		"#!>instruction \targument\t \n"
	};

	char *resInstPtr[] = {
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"IVOID",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",

		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",

		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",

		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction",
		"instruction"
	};

	char *resArgsPtr[] = {
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",
		"AVOID",

		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",

		"",
		"",
		" ",
		"",
		"",
		"",
		"",
		" ",

		"argument",
		"argument",
		"\targument",
		"argument ",
		"argument\t",
		"\targument\t ",

		"argument",
		"argument",
		"\targument",
		"argument ",
		"argument\t",
		"\targument\t "
	};

	char *instPtr = NULL;
	char *argsPtr = NULL;

	int i;
	int max = sizeof(moduleLines) / sizeof(char*);
	for (i = 0; i < max; i++) {
		instPtr = "IVOID";
		argsPtr = "AVOID";

		size_t slen = strlen(*(moduleLines + i));

		/* line must not be constant since it is manipulated by get_token */
		char *line = malloc(slen + 1);
		if (line == NULL) {
			perror("test_instruction_pointers");
			return;
		}
		strcpy(line, *(moduleLines + i));
		char *saveLine = line;

		init_filters();
		deactivate_all_filters();
		set_instruction_pointers(&line, &slen, &instPtr, &argsPtr);
		int cmpinstres = strcmp(instPtr, *(resInstPtr + i));
		int cmpargsres = strcmp(argsPtr, *(resArgsPtr + i));

		CU_ASSERT(cmpinstres == 0);
		CU_ASSERT(cmpargsres == 0);

		free(saveLine);

		release_stack2d_data(&filterStack);
		if (filters != NULL)
			free(filters);
	}
}


