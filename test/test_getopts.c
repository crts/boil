/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_getopts.h"



void test_set_module_pointers(void) {
	char *module[] = {
		"", /* 0 */
		"[header]",
		"[header][body]",
		"[header][body][footer]",
		"[header]\n",
		"[header]\n[body]\n",
		"[header]\n[body]\n[footer]\n",
		"[header]\ntext for head",
		"[header] \ntext for head[body]\ntext for body",
		"[header] \n\ttext for head \t[body]\ntext for body[footer]\ntext for foot",
		"[header]\ntext for head\n",
		"[header]\ntext for head\n[body]\ntext for body\n",
		"[header]\ntext for head\n[body]\ntext for body\n[footer]\ntext for foot\n", /* 12 */

		"  ",
		" [header] ",
		" [header][body] ",
		" [header][body][footer] ",
		" [header]\n ",
		" [header]\n[body]\n ",
		" [header]\n[body]\n[footer]\n ",
		" [header]\ntext for head ",
		" [header] \ntext for head[body]\ntext for body ",
		" [header] \n\ttext for head \t[body]\ntext for body[footer]\ntext for foot ",
		" [header]\ntext for head\n ",
		" [header]\ntext for head\n[body]\ntext for body\n ",
		" [header]\ntext for head\n[body]\ntext for body\n[footer]\ntext for foot\n ", /* 25 */

		"\n\n",
		"\n[header]\n",
		"\n[header][body]\n",
		"\n[header][body][footer]\n",
		"\n[header]\n\n",
		"\n[header]\n[body]\n\n",
		"\n[header]\n[body]\n[footer]\n\n",
		"\n[header]\ntext for head\n",
		"\n[header] \ntext for head[body]\ntext for body\n",
		"\n[header] \n\ttext for head \t[body]\ntext for body[footer]\ntext for foot\n",
		"\n[header]\ntext for head\n\n",
		"\n[header]\ntext for head\n[body]\ntext for body\n\n",
		"\n[header]\ntext for head\n[body]\ntext for body\n[footer]\ntext for foot\n\n" /* 38 */
	};

	char *expectHeader[] = {
		NULL,
		"",
		"",
		"",
		"",
		"",
		"",
		"text for head",
		"text for head",
		"\ttext for head \t",
		"text for head\n",
		"text for head\n",
		"text for head\n",


		NULL,
		"",
		"",
		"",
		" ",
		"",
		"",
		"text for head ",
		"text for head",
		"\ttext for head \t",
		"text for head\n ",
		"text for head\n",
		"text for head\n",


		NULL,
		"",
		"",
		"",
		"\n",
		"",
		"",
		"text for head\n",
		"text for head",
		"\ttext for head \t",
		"text for head\n\n",
		"text for head\n",
		"text for head\n"
	};

	char *expectBody[] = {
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"",
		"",
		NULL,
		"text for body",
		"text for body",
		NULL,
		"text for body\n",
		"text for body\n",


		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		" ",
		"",
		NULL,
		"text for body ",
		"text for body",
		NULL,
		"text for body\n ",
		"text for body\n",


		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"\n",
		"",
		NULL,
		"text for body\n",
		"text for body",
		NULL,
		"text for body\n\n",
		"text for body\n"
	};

	char *expectFooter[] = {
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"",
		NULL,
		NULL,
		"text for foot",
		NULL,
		NULL,
		"text for foot\n",


		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		" ",
		NULL,
		NULL,
		"text for foot ",
		NULL,
		NULL,
		"text for foot\n ",


		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"\n",
		NULL,
		NULL,
		"text for foot\n",
		NULL,
		NULL,
		"text for foot\n\n",

	};

	char *header, *body, *footer;
	char *input = NULL, *saveInput = NULL;

	int i;
	uint max = sizeof(module) / sizeof(char*);
	for (i = 0; i < max; i++) {
		input = strdup(*(module + i)); /* non const string is needed */
		saveInput = input; /* input will be mangled by set_module_pointers */

		size_t len = strlen(input);

		set_module_pointers(&input, &len, &header, &body, &footer);

		if ((header != NULL
		&& (*(expectHeader + i) == NULL || strcmp(header, *(expectHeader + i)) != 0))
		|| (header == NULL && *(expectHeader + i) != NULL)) {
			fprintf(stderr, "\nRun (header): %i\nExpect: <%s>\nActual: <%s>\n", i, *(expectHeader + i), header);
			CU_FAIL("");
		}

		if ((body != NULL
		&& (*(expectBody + i) == NULL || strcmp(body, *(expectBody + i)) != 0))
		|| (body == NULL && *(expectBody + i) != NULL)) {
			fprintf(stderr, "\nRun (body): %i\nExpect: <%s>\nActual: <%s>\n", i, *(expectBody + i), body);
			CU_FAIL("");
		}

		if ((footer != NULL
		&& (*(expectFooter + i) == NULL || strcmp(footer, *(expectFooter + i)) != 0))
		|| (footer == NULL && *(expectFooter + i) != NULL)) {
			fprintf(stderr, "\nRun (footer): %i\nExpect: <%s>\nActual: <%s>\n", i, *(expectFooter + i), footer);
			CU_FAIL("");
		}

		free(saveInput);
	}
}

void test_parse_getopts(void) {
	char *opts[] = {
		"",
		":",
		"a",
		":a",
		"a:",
		":a:",
		":ab",
		":ab:",
		":a:b",
		":a:b:",
		"ab",
		"ab:",
		"a:b",
		"a:b:"
	};

	char *result[] = {
		WHILE_DQ CASE ESAC,
		WHILE_DQ_C CASE ESAC,
		WHILE_DQ_A CASE_A ESAC,
		WHILE_DQ_CA CASE_A ESAC,
		WHILE_DQ_AC CASE_A ESAC,
		WHILE_DQ_CAC CASE_A ESAC,
		WHILE_DQ_CAB CASE_AB ESAC,
		WHILE_DQ_CABC CASE_AB ESAC,
		WHILE_DQ_CACB CASE_AB ESAC,
		WHILE_DQ_CACBC CASE_AB ESAC,
		WHILE_DQ_AB CASE_AB ESAC,
		WHILE_DQ_ABC CASE_AB ESAC,
		WHILE_DQ_ACB CASE_AB ESAC,
		WHILE_DQ_ACBC CASE_AB ESAC,
	};

	char *genericModule = NULL;
	char *name = "option";

	int i;
	int max = sizeof(opts) / sizeof(char*);
	for (i = 0; i < max; i++) {
		genericModule = NULL;

		char *module = strdup(GETOPTS_TEMPLATE); /* malloc() fresh module; will be mangled later on */
		size_t tlen = strlen(module);

		parse_getopts(module, tlen, &genericModule, *(opts + i), name);
		if (strcmp(genericModule, *(result + i)) != 0) {
			fprintf(stderr, "\nRun: %i\nExpected:\n^%s$\n-----\nActual:\n^%s$\n$---$\n", i, *(result + i), genericModule);
			CU_FAIL("");
		}

		free(module);
		if (genericModule != NULL)
			free(genericModule);
	}
}

