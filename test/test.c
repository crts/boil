/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "test.h"



extern int force; /* boolean */


/* The suite initialization function.
 * Returns zero on success, non-zero otherwise.
 */
int init_suite1(void) {
	return 0;
}

/* The suite cleanup function.
 * Returns zero on success, non-zero otherwise.
 */
int clean_suite1(void) {
	return 0;
}

/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main() {
	force = 1;
	CU_pSuite pSuite = NULL;

	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	/* add a suite to the registry */
	pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
	if (NULL == pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* add the tests to the suite */
	if (
	CU_add_test(pSuite, "test of kvMap_parse_line()", test_kvMap_parse_line) == NULL ||
	CU_add_test(pSuite, "test of parse_kvMap_add_pair()", test_kvMap_add_pair) == NULL ||
	CU_add_test(pSuite, "test of parse_getopts()", test_parse_getopts) == NULL ||
	CU_add_test(pSuite, "test of set_module_pointers()", test_set_module_pointers) == NULL ||
	CU_add_test(pSuite, "test of multi_repstr()", test_multi_repstr) == NULL ||
	CU_add_test(pSuite, "test of append_if_key_in_list()", test_append_if_key_in_list) == NULL ||
	CU_add_test(pSuite, "test of get_dual_delim_token()", test_get_dual_delim_token) == NULL ||
	CU_add_test(pSuite, "test of get_token()", test_get_token) == NULL ||
	CU_add_test(pSuite, "test of skip_chars()", test_skip_chars) == NULL ||
	CU_add_test(pSuite, "test of skip_until_chars()", test_skip_until_chars) == NULL ||
	CU_add_test(pSuite, "test of skip_order_chars()", test_skip_order_chars) == NULL ||
	CU_add_test(pSuite, "test of set_instruction_pointers()", test_set_instruction_pointers) == NULL ||
	CU_add_test(pSuite, "test of filter()", test_filter) == NULL) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* Run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();

	return CU_get_error();
}

