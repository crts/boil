/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_GETOPTS_H_INCLUDED
#define TEST_GETOPTS_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CUnit/Basic.h>


#include "../src/optparser/getopts.h"


/* definitions for test_parse_getopts */
#define WHILE_DQ "while getopts \"\" option; do\n"
#define WHILE_DQ_C "while getopts \":\" option; do\n"
#define WHILE_DQ_A "while getopts \"a\" option; do\n"
#define WHILE_DQ_CA "while getopts \":a\" option; do\n"
#define WHILE_DQ_AC "while getopts \"a:\" option; do\n"
#define WHILE_DQ_CAC "while getopts \":a:\" option; do\n"
#define WHILE_DQ_CAB "while getopts \":ab\" option; do\n"
#define WHILE_DQ_CABC "while getopts \":ab:\" option; do\n"
#define WHILE_DQ_CACB "while getopts \":a:b\" option; do\n"
#define WHILE_DQ_CACBC "while getopts \":a:b:\" option; do\n"
#define WHILE_DQ_AB "while getopts \"ab\" option; do\n"
#define WHILE_DQ_ABC "while getopts \"ab:\" option; do\n"
#define WHILE_DQ_ACB "while getopts \"a:b\" option; do\n"
#define WHILE_DQ_ACBC "while getopts \"a:b:\" option; do\n"

#define CASE "	case $option in\n"\

#define CASE_A CASE \
"	a) # TODO implement option a\n"\
"		;;\n"
#define CASE_AB CASE \
"	a) # TODO implement option a\n"\
"		;;\n"\
"	b) # TODO implement option b\n"\
"		;;\n"
#define ESAC "	*) # TODO implement unknown option\n"\
"		;;\n"\
"	esac\n"\
"done\n"

#define GETOPTS_TEMPLATE "[header]\n"\
"while getopts \"%opts%\" %var%; do\n"\
"	case $%var% in\n"\
"[body]\n"\
"	%o%) # TODO implement option %o\n"\
"		;;\n"\
"[footer]\n"\
"	*) # TODO implement unknown option\n"\
"		;;\n"\
"	esac\n"\
"done\n"


void test_set_module_pointers(void);
void test_parse_getopts(void);

void set_module_pointers(char**, size_t*, char**, char**, char**);
void parse_getopts(char*, size_t, char**, const char*, const char*);

#endif /* TEST_GETOPTS_H_INCLUDED */
