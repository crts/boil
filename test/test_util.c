/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_util.h"
#include "../src/instructions/map.h"


void test_skip_func_helper(const char *moduleline, const char **filterchars,
int *resoffset, int testruns, void (*testfunc) (const char**, size_t*, const char*, size_t)) {
	size_t modulelen = strlen(moduleline);

	char *line = malloc(modulelen + 1);
	strcpy(line, moduleline); /* line must not be constant */

	char *start = line;
	size_t len;

	int i;
	for (i = 0; i < testruns; i++) {
		line = start;
		len = modulelen;

		testfunc((const char**) &line, &len, *(filterchars + i), strlen(*(filterchars + i)));
		CU_ASSERT(strncmp(line, moduleline + *(resoffset + i), len) == 0);
		CU_ASSERT(len == modulelen - *(resoffset + i));
	}

	free(start);
}

void test_skip_chars(void) {
	const char *moduleline = "skipiks_keep";
	const char *filterchars[] = {"", "s", "skip"};
	int resoffset[] = {0, 1, 7};
	int testruns = sizeof(resoffset) / sizeof(int);

	test_skip_func_helper(moduleline, filterchars, resoffset, testruns, skip_chars);
}

void test_skip_until_chars(void) {
	const char *moduleline = "skip_that";
	const char *filterchars[] = {"", "_", "aht"};
	int resoffset[] = {9, 4, 5};
	int testruns = sizeof(resoffset) / sizeof(int);

	test_skip_func_helper(moduleline, filterchars, resoffset, testruns, skip_until_chars);
}

void test_skip_order_chars(void) {
	const char *moduleline = "#>_that";
	const char *filterchars[] = {"", ">", ">#", "#", "#>"};
	int resoffset[] = {0, 0, 0, 1, 2};
	int testruns = sizeof(resoffset) / sizeof(int);

	test_skip_func_helper(moduleline, filterchars, resoffset, testruns, skip_order_chars);
}

void test_get_token(void) {
	char *resultToken[] = {
		"VOID",
		"VOID",
		"VOID",
		"VOID",
		"VOID",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token",
		"token"
	};

	char *moduleLines[] = {
		"",
		" ",
		"\t",
		"\n",
		"\t \n",
		"token",
		" token",
		"\ttoken",
		"\t token",
		"token ",
		"token\t",
		"token\t ",
		"token\n",
		"token\t\n",
		"token\t \n",
		" token ",
		"\ttoken\t",
		"\t token\t\n",
		"\t token \n"
	};

	int newlen[] = {
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		1,
		0,
		1,
		2,
		0,
		0,
		1,
		1
	};

	char *result = NULL;

	int i;
	int max = sizeof(newlen) / sizeof(int);
	for (i = 0; i < max; i++) {
		size_t slen = strlen(*(moduleLines + i));
		result = "VOID";

		/* line must not be constant since it is manipulated by get_token */
		char *line = malloc(slen + 1);
		if (line == NULL) {
			perror("test_get_token()");
			return;
		}

		strcpy(line, *(moduleLines + i));

		char *saveLine = line;

		get_token(&line, &slen, &result);
		CU_ASSERT(strcmp(result, *(resultToken + i)) == 0);
		CU_ASSERT(slen == *(newlen + i));

		free(saveLine);
	}
}

void test_append_if_key_in_list(void) {
	char *result[] = {
		"",
		" ",
		"\n",
		" \n",
		"\n ",
		" \n ",
		"append here:",
		"append here: ",
		"append here:\n",
		"append here:\n ",
		"append here: \n"
	};

	struct kvMap *map = get_kvMap(2, "key", 3, "value", 5, "secondkey", 9, "val2", 4);

	if (map == NULL) {
		perror("test_append_if_key_in_list");
		return;
	}

	uint i, k;
	size_t max = sizeof(result) / sizeof(char*);
	for (i = 0; i < (*map).len; i++) {
		for (k = 0; k < max; k++) {
			size_t len = strlen(*(result + k));

			char *res = malloc(len + 1);
			strcpy(res, *(result + k));

			char *expectedResult = malloc(len + *((*map).vlen + i) + 1);
			*expectedResult = '\0';
			strcat(expectedResult, res);
			strcat(expectedResult, *((*map).val + i));

			int appended = append_if_key_in_list(&res, len, &len, 0, *((*map).key + i), *((*map).klen + i), map);
			CU_ASSERT_TRUE(appended == *((*map).vlen + i));
			CU_ASSERT_STRING_EQUAL(res, expectedResult);

			free(res);
			free(expectedResult);
		}
	}

	free_kvMap(map);
}

void test_multi_repstr(void) {
	char *line[] = {
		"",
		"key",
		"secondkey",
		"%%",
		"%%key",
		"%%secondkey",
		"key%%",
		"secondkey%%",
		"%%key%%",
		"%%secondkey%%",

		/* substitution */
		"%",
		"%nokey",
		"%nosecondkey",
		"%nokey at begin",
		"%secondnokey at begin",
		"at end %keyno",
		"at end %secondkeyno",
		"the %nokey in middle",
		"the %nosecondkey in middle",

		"%nokey %secondnokey",
		"%keyno and %secondkeyno",
		"both %nokey and %secondkeyno",
		"%nokey and %secondnokey present",

		"%key",
		"%secondkey",
		"%key at begin",
		"%secondkey at begin",
		"at end %key",
		"at end %secondkey",
		"the %key in middle",
		"the %secondkey in middle",
		"%key %secondkey",
		"%key and %secondkey",
		"both %key and %secondkey",
		"%key and %secondkey present",

		"%key%",
		"%secondkey%",
		"%key% at begin",
		"%secondkey% at begin",
		"at end %key%",
		"at end %secondkey%",
		"the %key% in middle",
		"the %secondkey% in middle",
		"%key% %secondkey%",
		"%key% and %secondkey%",
		"both %key% and %secondkey%",
		"%key% and %secondkey% present",

		"%key%",
		"%secondkey%",
		"%key%at begin",
		"%secondkey%at begin",
		"at end%key%",
		"at end%secondkey%",
		"the%key%in middle",
		"the%secondkey%in middle",
		"%key%%secondkey%",
		"%key%and%secondkey%",
		"both%key%and%secondkey%",
		"%key%and%secondkey%present",

		"%%key%%",
		"%%secondkey%%",
		"%%key%%at begin",
		"%%secondkey%%at begin",
		"at end%%key%%",
		"at end%%secondkey%%",
		"the%%key%%in middle",
		"the%%secondkey%%in middle",
		"%%key%%%%secondkey%%",
		"%%key%%and%%secondkey%%",
		"both%%key%%and%%secondkey%%",
		"%%key%%and%%secondkey%%present",

		/* whitespace */
		" % ",
		" %nokey ",
		" %nosecondkey ",
		" %nokey at begin ",
		" %secondnokey at begin ",
		" at end %keyno ",
		" at end %secondkeyno ",
		" the %nokey in middle ",
		" the %nosecondkey in middle ",

		" %nokey %secondnokey ",
		" %keyno and %secondkeyno ",
		" both %nokey and %secondkeyno ",
		" %nokey and %secondnokey present ",

		" %key ",
		" %secondkey ",
		" %key at begin ",
		" %secondkey at begin ",
		" at end %key ",
		" at end %secondkey ",
		" the %key in middle ",
		" the %secondkey in middle ",
		" %key %secondkey ",
		" %key and %secondkey ",
		" both %key and %secondkey ",
		" %key and %secondkey present ",

		" %key% ",
		" %secondkey% ",
		" %key% at begin ",
		" %secondkey% at begin ",
		" at end %key% ",
		" at end %secondkey% ",
		" the %key% in middle ",
		" the %secondkey% in middle ",
		" %key% %secondkey% ",
		" %key% and %secondkey% ",
		" both %key% and %secondkey% ",
		" %key% and %secondkey% present ",

		" %key% ",
		" %secondkey% ",
		" %key%at begin ",
		" %secondkey%at begin ",
		" at end%key% ",
		" at end%secondkey% ",
		" the%key%in middle ",
		" the%secondkey%in middle ",
		" %key%%secondkey% ",
		" %key%and%secondkey% ",
		" both%key%and%secondkey% ",
		" %key%and%secondkey%present ",

		" %%key%% ",
		" %%secondkey%% ",
		" %%key%%at begin ",
		" %%secondkey%%at begin ",
		" at end%%key%% ",
		" at end%%secondkey%% ",
		" the%%key%%in middle ",
		" the%%secondkey%%in middle ",
		" %%key%%%%secondkey%% ",
		" %%key%%and%%secondkey%% ",
		" both%%key%%and%%secondkey%% ",
		" %%key%%and%%secondkey%%present ",

		/* newlines */
		"\n%\n",
		"\n%nokey\n",
		"\n%nosecondkey\n",
		"\n%nokey at begin\n",
		"\n%secondnokey at begin\n",
		"\nat end %keyno\n",
		"\nat end %secondkeyno\n",
		"\nthe %nokey in middle\n",
		"\nthe %nosecondkey in middle\n",

		"\n%nokey %secondnokey\n",
		"\n%keyno and %secondkeyno\n",
		"\nboth %nokey and %secondkeyno\n",
		"\n%nokey and %secondnokey present\n",

		"\n%key\n",
		"\n%secondkey\n",
		"\n%key at begin\n",
		"\n%secondkey at begin\n",
		"\nat end %key\n",
		"\nat end %secondkey\n",
		"\nthe %key in middle\n",
		"\nthe %secondkey in middle\n",
		"\n%key %secondkey\n",
		"\n%key and %secondkey\n",
		"\nboth %key and %secondkey\n",
		"\n%key and %secondkey present\n",

		"\n%key%\n",
		"\n%secondkey%\n",
		"\n%key% at begin\n",
		"\n%secondkey% at begin\n",
		"\nat end %key%\n",
		"\nat end %secondkey%\n",
		"\nthe %key% in middle\n",
		"\nthe %secondkey% in middle\n",
		"\n%key% %secondkey%\n",
		"\n%key% and %secondkey%\n",
		"\nboth %key% and %secondkey%\n",
		"\n%key% and %secondkey% present\n",

		"\n%key%\n",
		"\n%secondkey%\n",
		"\n%key%at begin\n",
		"\n%secondkey%at begin\n",
		"\nat end%key%\n",
		"\nat end%secondkey%\n",
		"\nthe%key%in middle\n",
		"\nthe%secondkey%in middle\n",
		"\n%key%%secondkey%\n",
		"\n%key%and%secondkey%\n",
		"\nboth%key%and%secondkey%\n",
		"\n%key%and%secondkey%present\n",

		"\n%%key%%\n",
		"\n%%secondkey%%\n",
		"\n%%key%%at begin\n",
		"\n%%secondkey%%at begin\n",
		"\nat end%%key%%\n",
		"\nat end%%secondkey%%\n",
		"\nthe%%key%%in middle\n",
		"\nthe%%secondkey%%in middle\n",
		"\n%%key%%%%secondkey%%\n",
		"\n%%key%%and%%secondkey%%\n",
		"\nboth%%key%%and%%secondkey%%\n",
		"\n%%key%%and%%secondkey%%present\n"
	};

	char *expect[] = {
		"",
		"key",
		"secondkey",
		"%",
		"%key",
		"%secondkey",
		"key%",
		"secondkey%",
		"%key%",
		"%secondkey%",

		/* substitution */
		"",
		"",
		"",
		" at begin",
		" at begin",
		"at end ",
		"at end ",
		"the  in middle",
		"the  in middle",
		" ",
		" and ",
		"both  and ",
		" and  present",

		"value",
		"val2",
		"value at begin",
		"val2 at begin",
		"at end value",
		"at end val2",
		"the value in middle",
		"the val2 in middle",
		"value val2",
		"value and val2",
		"both value and val2",
		"value and val2 present",

		"value",
		"val2",
		"value at begin",
		"val2 at begin",
		"at end value",
		"at end val2",
		"the value in middle",
		"the val2 in middle",
		"value val2",
		"value and val2",
		"both value and val2",
		"value and val2 present",

		"value",
		"val2",
		"valueat begin",
		"val2at begin",
		"at endvalue",
		"at endval2",
		"thevaluein middle",
		"theval2in middle",
		"valueval2",
		"valueandval2",
		"bothvalueandval2",
		"valueandval2present",

		"%key%",
		"%secondkey%",
		"%key%at begin",
		"%secondkey%at begin",
		"at end%key%",
		"at end%secondkey%",
		"the%key%in middle",
		"the%secondkey%in middle",
		"%key%%secondkey%",
		"%key%and%secondkey%",
		"both%key%and%secondkey%",
		"%key%and%secondkey%present",

		/* whitespace */
		"  ",
		"  ",
		"  ",
		"  at begin ",
		"  at begin ",
		" at end  ",
		" at end  ",
		" the  in middle ",
		" the  in middle ",
		"   ",
		"  and  ",
		" both  and  ",
		"  and  present ",

		" value ",
		" val2 ",
		" value at begin ",
		" val2 at begin ",
		" at end value ",
		" at end val2 ",
		" the value in middle ",
		" the val2 in middle ",
		" value val2 ",
		" value and val2 ",
		" both value and val2 ",
		" value and val2 present ",

		" value ",
		" val2 ",
		" value at begin ",
		" val2 at begin ",
		" at end value ",
		" at end val2 ",
		" the value in middle ",
		" the val2 in middle ",
		" value val2 ",
		" value and val2 ",
		" both value and val2 ",
		" value and val2 present ",

		" value ",
		" val2 ",
		" valueat begin ",
		" val2at begin ",
		" at endvalue ",
		" at endval2 ",
		" thevaluein middle ",
		" theval2in middle ",
		" valueval2 ",
		" valueandval2 ",
		" bothvalueandval2 ",
		" valueandval2present ",

		" %key% ",
		" %secondkey% ",
		" %key%at begin ",
		" %secondkey%at begin ",
		" at end%key% ",
		" at end%secondkey% ",
		" the%key%in middle ",
		" the%secondkey%in middle ",
		" %key%%secondkey% ",
		" %key%and%secondkey% ",
		" both%key%and%secondkey% ",
		" %key%and%secondkey%present ",


		/* newlines */
		"\n\n",
		"\n\n",
		"\n\n",
		"\n at begin\n",
		"\n at begin\n",
		"\nat end \n",
		"\nat end \n",
		"\nthe  in middle\n",
		"\nthe  in middle\n",
		"\n \n",
		"\n and \n",
		"\nboth  and \n",
		"\n and  present\n",

		"\nvalue\n",
		"\nval2\n",
		"\nvalue at begin\n",
		"\nval2 at begin\n",
		"\nat end value\n",
		"\nat end val2\n",
		"\nthe value in middle\n",
		"\nthe val2 in middle\n",
		"\nvalue val2\n",
		"\nvalue and val2\n",
		"\nboth value and val2\n",
		"\nvalue and val2 present\n",

		"\nvalue\n",
		"\nval2\n",
		"\nvalue at begin\n",
		"\nval2 at begin\n",
		"\nat end value\n",
		"\nat end val2\n",
		"\nthe value in middle\n",
		"\nthe val2 in middle\n",
		"\nvalue val2\n",
		"\nvalue and val2\n",
		"\nboth value and val2\n",
		"\nvalue and val2 present\n",

		"\nvalue\n",
		"\nval2\n",
		"\nvalueat begin\n",
		"\nval2at begin\n",
		"\nat endvalue\n",
		"\nat endval2\n",
		"\nthevaluein middle\n",
		"\ntheval2in middle\n",
		"\nvalueval2\n",
		"\nvalueandval2\n",
		"\nbothvalueandval2\n",
		"\nvalueandval2present\n",

		"\n%key%\n",
		"\n%secondkey%\n",
		"\n%key%at begin\n",
		"\n%secondkey%at begin\n",
		"\nat end%key%\n",
		"\nat end%secondkey%\n",
		"\nthe%key%in middle\n",
		"\nthe%secondkey%in middle\n",
		"\n%key%%secondkey%\n",
		"\n%key%and%secondkey%\n",
		"\nboth%key%and%secondkey%\n",
		"\n%key%and%secondkey%present\n"
	};

	struct kvMap *map = get_kvMap(2, "key", 3, "value", 5, "secondkey", 9,
	"val2", 4);

	uint i;
	size_t max = sizeof(line) / sizeof(char*);
	for (i = 0; i < max; i++) {
		size_t len = strlen(*(line + i));

		char *str = malloc(len + 1); /* need malloc()'ed string */
		strcpy(str, *(line + i));

		char *result = multi_repstr('%', (const char*) str, len, map);

		if (result != NULL && strcmp(result, *(expect + i)) == 0) {
			CU_PASS("");
		} else {
			fprintf(stderr, "Loop iteration number %u failed.\nInput:    %s\nExpected: %s\nActual:   %s\n-----\n", i, str, *(expect + i), result);
			CU_FAIL("");
		}

		free(str);
		if (result != NULL)
			free(result);
	}

	free_kvMap(map);
}

void test_get_dual_delim_token(void) {
	char *strNoToken[] = {
		"",
		"\n",
		"[",
		"\n[",
		"[\n",
		"\n[\n",
		"no token",
		"half [token",
		"half [token is no token",
		"][",
		"Inverted delimiters in ]token[ do not count"
	};

	char *strEmptyToken[] = {
		"[]",
		"\n[]",
		"[]\n",
		"\n[]\n",
		"empty [] token"
	};

	size_t remainingLenEmptyToken[] = {
		0,
		0,
		1,
		1,
		6
	};

	char *strToken[] = {
		"[token]",
		"some[token]",
		"some [token]",
		"[token]text",
		"[token] text",
		"\n[token]",
		"[token]\n",
		"\n[token]\n"
	};

	size_t remainingLenToken[] = {
		0,
		0,
		0,
		4,
		5,
		0,
		1,
		1
	};

	char *token = "";

	int i;
	int max = sizeof(strNoToken) / sizeof(char*);
	for (i = 0; i < max; i++) {
		token = "";

		size_t len = strlen(*(strNoToken + i));
		size_t initLen = len;
		char *useStr = malloc(initLen + 1); /* need malloc'ed string */
		if (useStr == NULL) {
			perror("test_get_dual_delim_token()");
			return;
		}

		strcpy(useStr, *(strNoToken + i));

		char *initStr = useStr;

		get_dual_delim_token(&useStr, &len, DUAL_DELIM, &token);

		CU_ASSERT_PTR_NULL(token);
		CU_ASSERT(useStr == initStr); /* no address manipulation without token */
		CU_ASSERT(strcmp(*(strNoToken + i), useStr) == 0); /* no content manipulation without token */
		CU_ASSERT(len == initLen); /* no length manipulation without token */

		free(initStr);
	}

	max = sizeof(strEmptyToken) / sizeof(char*);
	for (i = 0; i < max; i++) {
		token = "";

		size_t len = strlen(*(strEmptyToken + i));
		char *useStr = malloc(len + 1); /* need malloc'ed string */
		if (useStr == NULL) {
			perror("test_get_dual_delim_token()");
			return;
		}

		strcpy(useStr, *(strEmptyToken + i));

		char *saveUseStr = useStr;

		get_dual_delim_token(&useStr, &len, DUAL_DELIM, &token);

		CU_ASSERT( *token == '\0'); /* empty token */
		CU_ASSERT(len == *(remainingLenEmptyToken + i)); /* remaining length of original string */

		free(saveUseStr);
	}

	max = sizeof(strToken) / sizeof(char*);
	for (i = 0; i < max; i++) {
		token = "";

		size_t len = strlen(*(strToken + i));
		char *useStr = malloc(len + 1); /* need malloc'ed string */
		if (useStr == NULL) {
			perror("test_get_dual_delim_token()");
			return;
		}

		strcpy(useStr, *(strToken + i));

		char *saveUseStr = useStr;

		get_dual_delim_token(&useStr, &len, DUAL_DELIM, &token);

		CU_ASSERT(strcmp(token, "token") == 0); /* token found */
		CU_ASSERT(len == *(remainingLenToken + i)); /* remaining length of original string */

		free(saveUseStr);
	}
}
