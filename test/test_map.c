/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_util.h"
#include "../src/instructions/map.h"

/* default mapped value for key */
#define VALUE "VALUE"
#define VLEN 5

void test_kvMap_add_pair(void) {
	char *strInvalidPair[] = {
		"",
		" ",
		"	",
		"key",
		"key with space",
		"=value",
		"=value with space"
	};

	char* strEmptyPair[] = {
		"key with space=",
		"key="
	};

	char* strValidPair[] = {
		"key=value",
		"key=value with space"
	};

	char* validResult[] = {
		"value",
		"value with space"
	};

	char* strMappedPair[] = {
		"key2=$<key>",
		"key2= $<key>",
		"key2=$<key> ",
		"key2=some $<key>",
		"key2=$<key> stored",

		"key2=\\$<key>",
		"key2= \\$<key>",
		"key2=\\$<key> ",
		"key2=some \\$<key>",
		"key2=\\$<key> stored",

		"key2=<key>",
		"key2= <key>",
		"key2=<key> ",
		"key2=some <key>",
		"key2=<key> stored",

	};

	char* value = NULL;

	int i;
	int max = sizeof(strInvalidPair) / sizeof(char*);
	for (i = 0; i < max; i++) {
		value = NULL;

		size_t len = strlen(*(strInvalidPair + i));

		char *useStr = malloc(len + 1); /* need malloc'ed string */
		if (useStr == NULL) {
			perror("test_kvMap_add_pair()");
			return;
		}

		strcpy(useStr, *(strInvalidPair + i));

		struct kvMap* map = get_kvMap(0); /* empty map */
		kvMap_add_pair(map, useStr, len);

		kvMap_getvalue(map, useStr, &value);
		CU_ASSERT((*map).len == 0);
		CU_ASSERT_PTR_NULL(value);

		free(useStr);
		free_kvMap(map);
	}

	max = sizeof(strEmptyPair) / sizeof(char*);
	for (i = 0; i < max; i++) {
		value = NULL;

		size_t len = strlen(*(strEmptyPair + i));

		char *useStr = malloc(len + 1); /* need malloc'ed string */
		if (useStr == NULL) {
			perror("test_kvMap_add_pair()");
			return;
		}

		strcpy(useStr, *(strEmptyPair + i));

		struct kvMap* map = get_kvMap(0); /* empty map */
		kvMap_add_pair(map, useStr, len);

		kvMap_getvalue(map, useStr, &value);
		CU_ASSERT((*map).len == 1);
		CU_ASSERT_STRING_EQUAL(value, "");

		free(useStr);
		if (value) free(value);
		free_kvMap(map);
	}

	max = sizeof(strValidPair) / sizeof(char*);
	for (i = 0; i < max; i++) {
		value = NULL;

		size_t len = strlen(*(strValidPair + i));

		char *useStr = malloc(len + 1); /* need malloc'ed string */
		if (useStr == NULL) {
			perror("test_kvMap_add_pair()");
			return;
		}

		strcpy(useStr, *(strValidPair + i));

		struct kvMap* map = get_kvMap(0); /* empty map */

		kvMap_add_pair(map, useStr, len);
		CU_ASSERT((*map).len == 1);

		kvMap_getvalue(map, "key", &value);
		CU_ASSERT_STRING_EQUAL(value, *(validResult + i));

		if (value) free(value);
		free(useStr);
		free_kvMap(map);
	}

}

void test_kvMap_parse_line(void) {
	const char* strMappedPair[] = {
		"$<key>",
		" $<key>",
		"$<key> ",
		"some $<key>",
		"$<key> stored",
		"some $<key> stored",

		"\\$<key>",
		" \\$<key>",
		"\\$<key> ",
		"some \\$<key>",
		"\\$<key> stored",
		"some \\$<key> stored",

		"$>key<",
		" $>key<",
		"$>key< ",
		"some $>key<",
		"$>key< stored",
		"some $>key< stored",

		"<key>",
		" <key>",
		"<key> ",
		"some <key>",
		"<key> stored",
		"some <key> stored",

		"$key>",
		" $key>",
		"$key> ",
		"some $key>",
		"$key> stored",
		"some $key> stored",

		"$<key",
		" $<key",
		"$<key ",
		"some $<key",
		"$<key stored",
		"some $<key stored",

		"$<",
		"\\$<",
		"$",
		"<",
		"$<>",
		"<>",

		"some $<",
		"some \\$<",
		"some $",
		"some <",
		"some $<>",
		"some <>",

		"$< value",
		"\\$< value",
		"$ value",
		"< value",
		"$<> value",
		"<> value",

		"\\<key>",
		" \\<key>",
		"\\<key> ",
		"some \\<key>",
		"\\<key> stored",
		"some \\<key> stored",

		"\\",
		"some \\",
		"\\ value",
		""
	};

	const char* strMappedResult[] = {
		VALUE,
		" " VALUE,
		VALUE " ",
		"some " VALUE,
		VALUE " stored",
		"some "VALUE " stored",

		"$<key>",
		" $<key>",
		"$<key> ",
		"some $<key>",
		"$<key> stored",
		"some $<key> stored",

		"$>key<",
		" $>key<",
		"$>key< ",
		"some $>key<",
		"$>key< stored",
		"some $>key< stored",

		"<key>",
		" <key>",
		"<key> ",
		"some <key>",
		"<key> stored",
		"some <key> stored",

		"$key>",
		" $key>",
		"$key> ",
		"some $key>",
		"$key> stored",
		"some $key> stored",

		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,

		NULL,
		"$<",
		"$",
		"<",
		NULL,
		"<>",

		NULL,
		"some $<",
		"some $",
		"some <",
		NULL,
		"some <>",

		NULL,
		"$< value",
		"$ value",
		"< value",
		NULL,
		"<> value",

		"\\<key>",
		" \\<key>",
		"\\<key> ",
		"some \\<key>",
		"\\<key> stored",
		"some \\<key> stored",

		"\\",
		"some \\",
		"\\ value",
		""
	};

	char* parsed = NULL;

	char* keyval = strdup("key=" VALUE);
	size_t klen = 3;

	struct kvMap* map = get_kvMap(0); /* empty map */
	kvMap_add_pair(map, keyval, strlen(keyval));

	const char* expect = NULL;
	int max = sizeof(strMappedPair) / sizeof(char*);
	for (int i = 0; i < max; i++) {
		parsed = NULL;

		size_t len = strlen(*(strMappedPair + i));

		char *line = malloc(len + 1); /* need malloc'ed string */
		if (line == NULL) {
			perror("test_kvMap_parse_line()");
			return;
		}

		strcpy(line, *(strMappedPair + i));
		char* save = line;

		size_t plen = kvMap_parse_line(map, &line, len, &parsed);

		expect = *(strMappedResult + i);
		if (expect == NULL) {
			CU_ASSERT_PTR_NULL(parsed);
		} else {
			CU_ASSERT_STRING_EQUAL(parsed, expect);
			free(parsed);
		}
		free(save);
	}

	free_kvMap(map);
	free(keyval);
}
