/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CUnit/Basic.h>


#include "../src/process.h"
#include "../src/util.h"
#include "../src/prepenv.h"
#include "../src/optparser/getopts.h"

#include "test_util.h"
#include "test_map.h"
#include "test_getopts.h"
#include "test_process.h"
#include "test_filter.h"

#endif /* TEST_H_INCLUDED */
