--- submodule-stop.mod ---

#!> STOP filter

# first comment
#! first hashbang

#!> START filter

# mid comment
#! mid hashbang

#!> STOP filter

# final comment
#! final hashbang
