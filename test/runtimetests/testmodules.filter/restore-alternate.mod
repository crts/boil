--- restore-alternate.mod ---

setting status and laststatus to 0
#!> FILTER hashbang=0
#!> FILTER hashbang=0
#!> FILTER comment=0
#!> FILTER comment=0

#!> FILTER comment=1
filter
hashbang=0
comment=1
#!/shebang
# first parent comment
#! first parent hashbang

#!> RESTORE filter

restore 1
hashbang=0
comment=0
#!/shabang
# second parent comment
#! second parent hashbang

#!> RESTORE filter

restore 2
hashbang=0
comment=1
#!/hashbang
# third parent comment
#! third parent hashbang

#!> FILTER hashbang=1
#!> FILTER comment=0

filter
hashbang=1
comment=0
#!/shebang_2
# fourth parent comment
#! fourth parent hashbang

#!> RESTORE filter

restore 3
hashbang=0
comment=1
#!/shabang_2
# last parent comment
#! last parent hashbang

