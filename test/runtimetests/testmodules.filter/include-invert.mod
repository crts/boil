--- include-invert.mod ---
#!/shabang

# first parent comment
#! first parent hashbang

#!> MULTIPASS ./default.mod

#!> FILTER comment=1
#!> MULTIPASS ./default.mod
#!> FILTER comment=0

# second parent comment
#! second parent hashbang

#!> FILTER hashbang=1
#!> MULTIPASS ./default.mod
#!> FILTER hashbang=0

# third parent comment
#! third parent hashbang

#!> FILTER hashbang=1
#!> FILTER comment=1
#!> MULTIPASS ./default.mod
#!> FILTER hashbang=0
#!> FILTER comment=0

# last parent comment
#! last parent hashbang

