--- include-stop.mod ---
#!/shabang

#!> MULTIPASS ./submodule-stop.mod

# first parent comment
#! first parent hashbang

#!> STOP filter
#!> MULTIPASS ./submodule-start.mod

# second parent comment
#! second parent hashbang

# last parent comment
#! last parent hashbang

