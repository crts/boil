--- restore.mod ---
#!> STOP filter

#!> START filter

#!/shebang

# first parent comment
#! first parent hashbang

#!> RESTORE filter

#!/hashbang

# second parent comment
#! second parent hashbang

#!> RESTORE filter

#!/shabang

# last parent comment
#! last parent hashbang


