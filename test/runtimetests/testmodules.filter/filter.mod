--- filter.mod ---

# first comment
#! first hashbang

#!> FILTER comment=1
# second comment
#! second hashbang
#!> FILTER comment=0

# third comment
#! third hashbang

#!> FILTER hashbang=1
# fourth comment
#! fourth hashbang
#!> FILTER hashbang=0

# fifth comment
#! fifth hashbang

#!> FILTER hashbang=1
#!> FILTER comment=1
# sixth comment
#! sixth hashbang
#!> FILTER hashbang=0
#!> FILTER comment=0

# final comment
#! final hashbang
