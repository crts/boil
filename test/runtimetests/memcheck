#!/usr/bin/env bash

# Test script for boil
# 
# Copyright (C) 2021- CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

shopt -s extglob

resolve_path() {(
	unset CDPATH
	[[ -e "$1" ]] || return 1
	if [[ -d "$1" ]];then
		cd "$1" && pwd
	else
		local -r path=$(get_path "$1")
		local -r file=$(get_filename "$1")
		cd "$path" || return 1
		echo "$(pwd)/$file"
	fi
)}

is_explicit_path() {
	[[ ${1:0:1} == '/' || ${1:0:2} == './' || ${1:0:3} == '../' ]]
}

# Replaces all multiple consecutive occurences of '//' with single '/'.
# If the string contains a trailing '/' then it is also stripped.
norm_path() {(
	shopt -s extglob

	if [[ -n "$1" ]];then
		local v="${1//+(\/)//}"

		if [[ "$v" == "/" ]];then
			echo "$v"
		else
			echo "${v%/}"
		fi
	fi
)}

# Returns the normalized path of the parent directory:
#
#  $ get_path "/path/to///folder//subdir_or_filename"
#  /path/to/folder
#  $ get_path "path/to/folder//subdir/"
#  path/to/folder
get_path() {
	if [[ -n "$1" ]];then
		local w=$(norm_path "$1")
		local v="${w%/*}"

		if [[ "$v" == "$w" ]];then
			echo "."
		else
			if [[ -n "$v" ]];then
				echo "$v"
			else
				echo "/"
			fi
		fi
	fi
}

# Returns the normalized last component of the path:
#
#  $ get_filename "path/to/filename"
#  filename
#  $ get_filename "/path/to/folder/"
#  folder
get_filename() {
	if [[ -n "$1" ]];then
		local v=$(norm_path "$1")
		v="${v##*/}"
		if [[ -z "$v" ]];then
			echo "/"
		else
			echo "$v"
		fi
	fi
}

declare -a _DELETION_PATHS_

add_del_paths() {
	local -i _last=${#_DELETION_PATHS_[@]}

	while (( $# ));do
		_DELETION_PATHS_[_last]=$(realpath "$1")
		(( _last++ )) || :
		shift
	done
}

set_del_paths() {
	_DELETION_PATHS_=()
	add_del_paths "$@"
}

reset_del_paths() {
	set_del_paths "/tmp"
}

reset_del_paths

del() {
	local _file

	local -a _option
	local -i _last=0
	while [[ "${1:0:1}" == "-" ]];do
		_option[_last]="$1"
		(( _last++ )) || :
		if [[ "$1" == "--" ]];then
			shift
			break
		fi
		shift
	done

	local _deldir
	local -a _list
	_last=0
	while (( $# ));do
		_file=$(realpath "$1")
		for _deldir in "${_DELETION_PATHS_[@]}";do
			if [[ "${_file#$_deldir}" != "$_file" ]];then
				# file inside allowed deletion path
				_list[_last]="$_file"
				(( _last++ )) || :
				break
			fi
		done

		shift
	done

	if (( ${#_list[@]} ));then
		rm "${_option[@]}" "${_list[@]}"
	else
		return 1
	fi
}

getPath() {
	if [[ -n "$1" ]];then
		local -r v="${1//+(\/)//}"
		echo "${v%/*}"
	fi
}

print_stats() {
# $1 log file to extract stats

	if [[ ! -f "$1" ]];then
		echo "Cannot print stats. File not found: $1" >&2
		return 1
	fi

	sync
	
	local -i numpass=$(grep "PASS" "$1" | wc -l)
	local -i numfail=$(grep "FAIL" "$1" | wc -l)
	local -i numtest=$(grep "$FINALIZER" "$1" | wc -l)
	
	echo "Tests: $numtest"
	echo "PASS: $numpass"
	echo "FAIL: $numfail"

}

declare -r MYPATH=$(getPath "$0")
declare -r MYNAME="${0##*/}"

if (( $(id -u) == 0 || $(id -g) == 0 ));then
	echo "Tests must not be run as root! Abort." >&2
	exit 1
fi

declare -r TESTUSERFILE="$MYPATH/testuser"
if [[ ! -f "$TESTUSERFILE" ]];then
	echo "No test user defined. Abort." >&2
	exit 1
fi

declare -r TESTUSER=$(head -n1 "$TESTUSERFILE")
if [[ "$USER" != "$TESTUSER" ]];then
	echo "Current user is not authorized to run tests. Abort." >&2
	exit 1
fi

MODULE="$1"

declare -r TESTBINARY=boil
declare -r BINPATH="$MYPATH/../../build"

declare -r BINTESTFILE="$BINPATH/$TESTBINARY"

declare -r MODULEDIR="testmodules.$MODULE"
declare -r MODULEPATH="$MYPATH/$MODULEDIR"
declare -r LOGDIR="$MYPATH/logs"

add_del_paths "$LOGDIR"

declare LOG

declare -r FINALIZER="=========="

if [[ ! -d "$MODULEPATH" ]];then
	echo "Testmodules missing. Cannot perform runtime tests." >&2
	exit 1
fi

source "$MYPATH/common.$MODULE"

mkdir -p "$LOGDIR"

declare -a logs
declare -a summary

for PREFIX in "" "${PREFIXES[@]}";do
	LOG="$LOGDIR/leak-$MODULE.log"
	SUMMARY="$LOGDIR/$PREFIX/leak-$MODULE.summary"
	mkdir -p "$LOGDIR/$PREFIX"

	del -f "$LOG" "$SUMMARY"

	logs[${#logs[@]}]="$LOG"
	summary[${#summary[@]}]="$SUMMARY"

	echo "Running [$MODULE] tests for PREFIX: $PREFIX" | tee -a "$LOG"

	for o in "${opts[@]}";do
		command="$BINTESTFILE ${PREFIX:+-p $PREFIX} -m $MODULEPATH $o"
		command=$(sed -r 's/ +/ /g' <<< "$command") # replace multiple space with one
		echo "$command" | tee -a "$SUMMARY"

		valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes $command 2>&1 >/dev/null | tee -a "$LOG" |  awk '{if ($0 ~ "total heap"){if ($5==$7){print "PASS"}else{print "FAIL"}}}' | tee -a "$SUMMARY"

		echo -e "$FINALIZER\n" >> "$LOG"
		echo "$FINALIZER" | tee -a "$SUMMARY"
	done
done

for s in "${summary[@]}";do
	echo "Statistics: $l"
	print_stats "$s"
done



