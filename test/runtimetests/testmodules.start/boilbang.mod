--- boilbang.mod ---

do not display NOP instruction:
#!> NOP
was it displayed?

#!> RESTORE boilbang

do not display NOP instruction:
#!> NOP
was it displayed?

stop boilbang
#!> STOP boilbang

display NOP instruction:
#!> NOP

#!> START boilbang
do not display NOP instruction:
#!> NOP
was it displayed?

#!> RESTORE boilbang
display NOP instruction:
#!> NOP

#!> RESTORE boilbang
do not display NOP instruction:
#!> NOP
was it displayed?

