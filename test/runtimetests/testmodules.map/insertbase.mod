--- insertbase.mod ---
#!> MAP key_a=insert_a.mod
#!> MAP key_b=insert_b.mod

Inserting with parent key module A:
#!> INSERT $<key_a>

Inserting with parent key module B:
#!> INSERT $<key_b>

#!>MAP key=INSERT_VALUE

Inserting with map key module A:
#!> INSERT insert_a.mod

Inserting with map key module B:
#!> INSERT insert_b.mod

Missing key:
#!> INSERT $<nokey>
end
