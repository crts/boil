--- default.mod ---

Normal line

#!> REPLACE $<key>
#!> REPLACE begin  $<key>
#!> REPLACE $<key> end

#!> REPLACE \$<key>
#!> REPLACE begin \$<key>
#!> REPLACE \$<key> end

#!> REPLACE $key>
#!> REPLACE begin $key>
#!> REPLACE $key> end

#!> REPLACE $<key
#!> REPLACE begin $<key
#!> REPLACE $<key end

#!> REPLACE $<>
#!> REPLACE begin $<>
#!> REPLACE $<> end

#!> REPLACE No variable replacement \${key} just regular \$
#!> REPLACE No escapes for ${key} required; neither for regular $

Do no replace $<key>
last line

