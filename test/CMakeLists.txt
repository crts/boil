
set( C_TEST_SRC
	test.c
	test_getopts.c
	test_process.c
	test_util.c
	test_map.c
	test_filter.c
	../src/prepenv.c
	../src/process.c
	../src/util.c
	../src/optparser/getopts.c
	../src/instructions/start.c
	../src/instructions/map.c
	../src/filter/filter.c
	../src/filter/filter_comment.c
	../src/filter/filter_hashbang.c
)

add_executable(testboil ${C_TEST_SRC})

target_link_libraries(testboil cunit)

