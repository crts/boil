/* Boil assists in the inclusion of boilerplate code for Shell scripts.
 *
 * Copyright (C) 2021- CRTS
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <CUnit/Basic.h>

#include "../src/filter/filter.h"
#include "../src/util.h"

extern int debuglevel;
extern struct stack2d filterStack;
extern struct sfilter* filters;


static char *moduleLines[] = {
	"#!>",
	" #!>",
	"\t#!>",
	" \t#!>",

	"",
	" ",
	"\t",
	" \t",

	"#",
	" #",
	"\t#",
	" \t#",
	"#-",
	" #-",
	"\t#-",
	" \t#-"

	"#!",
	" #!",
	"\t#!",
	" \t#!",
	"#!-",
	" #!-",
	"\t#!-",
	" \t#!-"
};

static void test_filter_all() {
	debuglevel = 1;

	int expect[] = {
		0,
		0,
		0,
		0,

		0,
		0,
		0,
		0,

		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,

		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1
	};

	int i;
	int max = sizeof(moduleLines) / sizeof(char*);
	for (i = 0; i < max; i++) {
		size_t slen = strlen(*(moduleLines + i));

		/* line must not be constant since it is manipulated by get_token */
		char *line = malloc(slen + 1);
		if (line == NULL) {
			perror("test_instruction_pointers");
			return;
		}
		strcpy(line, *(moduleLines + i));
		char *saveLine = line;

		init_filters();

		int filterres = apply_filters(&line, &slen);

		CU_ASSERT(filterres == expect[i]);

		free(saveLine);

		release_stack2d_data(&filterStack);
		if (filters != NULL)
			free(filters);
	}
}

static void test_filter_hashbang() {
	debuglevel = 1;

	int expect[] = {
		0,
		0,
		0,
		0,

		0,
		0,
		0,
		0,

		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,

		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1
	};

	int i;
	int max = sizeof(moduleLines) / sizeof(char*);
	for (i = 0; i < max; i++) {
		size_t slen = strlen(*(moduleLines + i));

		/* line must not be constant since it is manipulated by get_token */
		char *line = malloc(slen + 1);
		if (line == NULL) {
			perror("test_instruction_pointers");
			return;
		}
		strcpy(line, *(moduleLines + i));
		char *saveLine = line;

		init_filters();
		deactivate_all_filters();

		char *filterexpr = strdup(S_HASHBANG "=1");
		parse_filter(filterexpr);

		int filterres = apply_filters(&line, &slen);
		CU_ASSERT(filterres == expect[i]);

		free(filterexpr);
		free(saveLine);

		release_stack2d_data(&filterStack);
		if (filters != NULL)
			free(filters);
	}
}

static void test_filter_comment() {
	debuglevel = 1;

	int expect[] = {
		0,
		0,
		0,
		0,

		0,
		0,
		0,
		0,

		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,

		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	};

	int i;
	int max = sizeof(moduleLines) / sizeof(char*);
	for (i = 0; i < max; i++) {
		size_t slen = strlen(*(moduleLines + i));

		/* line must not be constant since it is manipulated by get_token */
		char *line = malloc(slen + 1);
		if (line == NULL) {
			perror("test_instruction_pointers");
			return;
		}
		strcpy(line, *(moduleLines + i));
		char *saveLine = line;

		init_filters();
		deactivate_all_filters();

		char *filterexpr = strdup(S_COMMENT "=1");
		parse_filter(filterexpr);

		int filterres = apply_filters(&line, &slen);
		CU_ASSERT(filterres == expect[i]);

		free(filterexpr);
		free(saveLine);

		release_stack2d_data(&filterStack);
		if (filters != NULL)
			free(filters);
	}
}

void test_filter() {
	test_filter_comment();
	test_filter_hashbang();
	test_filter_all();
}
