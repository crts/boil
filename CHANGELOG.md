## [1.2] 2021-03-04

### Changed
- Runtimetests now require a dedicated testuser account
- Renamed RESUME and SUSPEND to START and STOP
- Changed the way filters are processed. This will make it easier to
  implement additional filters and funcionality that requires filters, if
  the need arises in the future. Each filter now resides in its
  own file. They can be individually activated or deactivated.

### Added
- Licence notice in all source files
- Map a key/value pair by commandline option
- MAP instruction allows to map a key/value pair inside script
- REPLACE instruction replaces all occurences of $<key> in its
   argument with its mapped value.
- INSERT inserts a module. All occurences of $<key> are automatically
  replace in an inserted module.
- STOP instruction to stop all filters at once
- START instruction to start all filters at once
- New unit and runtime tests for filters and RESUME/SUSPEND instructions
- Started this CHANGELOG

