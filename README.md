Boil is a tool intended to assist in inclusion of
boilerplate code for shell scripts.

Boil is published under the GPLv3 license. You should
have received a copy of the license.

